const store = {
    currentOp: null,
    displayValue: "",
    firstInput: null,
};

function inputValue (num) {
    store.displayValue = `${store.displayValue}${num}`;
    updateDisplay();
};

function updateDisplay () {
    document.getElementById("display").value = store.displayValue;
    console.log(store);
}

function clearDisplay () {
    store.displayValue = "";
    updateDisplay();
}

function startOp (op) {
    store.currentOp = op;
    store.firstInput = store.displayValue;
    store.displayValue = "";
    console.log(store);
}

function calculate () {
    switch (store.currentOp) {
        case "+":
            store.displayValue = String(Number(store.firstInput) + Number(store.displayValue));
            break;
        case "-":
            store.displayValue = String(Number(store.firstInput) - Number(store.displayValue));
            break;
        case "*":
            store.displayValue = String(Number(store.firstInput) * Number(store.displayValue));
            break;
        case "/":
            store.displayValue = String(Number(store.firstInput) / Number(store.displayValue));
            break;
        default:
            store.displayValue = "Invalid Operation";
            break;
    }
    
    updateDisplay();
}