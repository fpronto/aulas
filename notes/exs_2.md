# Ficha 1

## Objetivos:

O objetivo desta ficha é aplicar os conhecimentos sobre `If` e `Switch...Case`

## Exercícios
 
1. Desenvolve um programa que peça um caracter ao utilizador e indique se o caracter é maiúsculo, minúsculo ou outro.

1. Desenvolve um programa que após ler as coordenadas de um ponto do plano na forma de par ordenado (x, y) determine a que quadrante pertence.

1. Desenvolve um programa que peça a hora atual (Sem minutos) ao utilizador no formato 0 - 24 horas e escreva no monitor a hora no formato AM - PM.
   - Hora atual → 17
   - Escreve no monitor → 5 PM

1. Desenvolve um programa que peça ao utilizador o tipo e a idade do veículo e imprima o imposto a pagar de acordo com a seguinte tabela:

| Tipo | >= 5 anos | < 5 anos |
| ---- | ---- | ---- |
| 1 | €25 | €45 |
| 2 | €50 | €125 |
| 3 | €75 | €150 |
| 4 | €150 | €250 |

5. Desenvolve um programa que leia três valores inteiros e determine se estes podem corresponder aos lados de um triângulo. Além disso, se os valores corresponderem aos lados de um triângulo, este deve ser classificado como equilátero, isósceles ou escaleno (um triângulo é equilátero se tiver os lados todos iguais, isósceles se dois lados forem iguais e escaleno se todos os lados tiverem comprimentos diferentes).

6. Desenvolve um programa que efetue a leitura de 3 valores inteiros e escreva o maior no monitor.

7. Desenvolve um programa que efetue a leitura de três valores inteiros e os escreva no monitor por ordem crescente.