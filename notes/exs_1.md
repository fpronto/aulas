# Ficha 1

## Objetivos:

O objetivo desta ficha é aplicar os conhecimentos sobre variáveis

## Exercícios
 
1. Desenvolve um programa que leia o valor do raio de uma circunferência e imprime a sua área e o seu perímetro.

2. Desenvolve um programa que leia os valores dos lados de um retângulo e imprime a sua área.

3. Desenvolve um programa que, dados os comprimentos dos catetos de um triângulo retângulo, imprime o comprimento da hipotenusa e o valor da área.

4. Desenvolve um programa que converta uma temperatura lida em graus Fahrenheit para graus Célsius. Utiliza a fórmula indicada para efetuar a conversão:  C = 5/9(f − 32)

5. Desenvolve um programa que, dados os valores de duas variáveis inteiras A e B, troque esses valores entre si.

6. Desenvolve um programa que calcule a quantidade de litros de combustível gastos numa viagem de automóvel. Assume que o automóvel tem um consumo de 6.5 litros por cada 100 km percorridos e que a velocidade é constante durante toda a viagem. O programa deve perguntar ao utilizador qual a duração da viagem e qual a velocidade atingida e escrever no final a distância percorrida e os litros de combustível consumidos. A fórmula para calcular a distância percorrida (considerando a velocidade constante) é a seguinte: Distância percorrida = velocidade × duração