import React from "react";

import useMediaQuery from "@mui/material/useMediaQuery";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { ThemeProvider } from "@mui/material/styles";
import blue from "@mui/material/colors/blue";
import { createTheme } from "@mui/material/styles";


import App from "./App";
import Quiz from "./Quiz";
import "./index.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "/quiz",
    element: <Quiz />,
  },
]);

const Entry = () => {
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");
  const theme = createTheme({
    mode: prefersDarkMode ? "dark" : "light",
    palette: {
      primary: blue,
    },
  });

  return (
    <React.StrictMode>
      <ThemeProvider theme={theme}>

        <RouterProvider router={router} />
      </ThemeProvider>

    </React.StrictMode>
  )
}

export default Entry;



// @media(prefers - color - scheme: dark) {
//   :root {
//     --background - color: #333333; /* dark mode */
//     --text - color: #FFFFFF; /* dark mode */
//   }
//   body {
//     background - color: var(--background - color);
//     color: var(--text - color);
//   }
// }

// @media(prefers - color - scheme: light) {
//   :root {
//     --background - color: #FFFFFF; /* light mode */
//     --text - color: #333333; /* light mode */
//   }
//   body {
//     background - color: var(--background - color);
//     color: var(--text - color);
//   }
// }
