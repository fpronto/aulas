import React from "react";
import ReactDOM from "react-dom/client";
import Entry from './Entry'


ReactDOM.createRoot(document.getElementById("root")).render(
  <Entry />
);
