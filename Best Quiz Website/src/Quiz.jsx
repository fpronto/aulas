import React, { useState } from "react";
import LinearProgress from '@mui/material/LinearProgress';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

import answers from "../mock/quiz.json";
import { QuizItem } from "./QuizItem";

const LinearProgressWithLabel = (props) => {
  return (
    <Box sx={{ display: 'flex', alignItems: 'center' }}>
      <Box sx={{ width: '100%', mr: 1 }}>
        <LinearProgress variant="determinate" {...props} />
      </Box>
      <Box sx={{ minWidth: 35 }}>
        <Typography variant="body2">{`${Math.round(
          props.value,
        )}%`}</Typography>
      </Box>
    </Box>
  );
}



const Quiz = () => {
  const answer = answers.results;

  const [startGame, setStartGame] = useState(false);
  const [questionIndex, setQuestionIndex] = useState(0);
  const [correctAnswerCount, setCorrectAnswerCount] = useState(0);

  const nextQuestion = (isCorrectAnswer) => {
    isCorrectAnswer && setCorrectAnswerCount(correctAnswerCount + 1);
    if (questionIndex + 1 <= answers.results.length) {
      setQuestionIndex(questionIndex + 1);
    }
  };

  const progress = questionIndex * 100 / answer.length;
  return (
    <div>
      {!startGame && (
        <button
          onClick={() => {
            setStartGame(true);
          }}
        >
          Start Quiz
        </button>
      )}
      {startGame && (
        <div>
          <LinearProgressWithLabel value={progress} />
          <span css={{
            fontSize: "2rem"
          }}>Question #{questionIndex + 1}</span>
          <QuizItem
            questionData={answer[questionIndex]}
            nextQuestion={nextQuestion}
          />
          <br />
          <span>
            Correct Questions: {correctAnswerCount}/{questionIndex}
          </span>
        </div>
      )
      }
    </div >
  );
};

export default Quiz;