import React, { useState } from "react";

export const QuizItem = ({ questionData, nextQuestion }) => {
  const [selectedAnswer, setSelectedAnswer] = useState();
  const [answerBlock, setAnswerBlock] = useState(false);

  const handleOnChange = (event) => {
    if (!answerBlock) {
      setSelectedAnswer(event.target.value);
    }
  };

  const shuffleArray = (array) => {
    const newArray = [...array];
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return newArray;
  };

  // questionData = {
  //   "category": "Science & Nature",
  //   "type": "multiple" || "boolean",
  //   "difficulty": "medium",
  //   "question": "Gannymede is the largest moon of which planet?",
  //   "correct_answer": "Jupiter",
  //   "incorrect_answers": [
  //       "Uranus",
  //       "Neptune",
  //       "Mars"
  //   ]
  // }

  const answers =
    questionData.type === "multiple"
      ? shuffleArray([
        ...questionData.incorrect_answers,
        questionData.correct_answer,
      ])
      : ["True", "False"];

  return (
    <div>
      <div
        css={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          margin: "3rem",
        }}
      >
        <h1>{questionData.question}</h1>
        {questionData.type === "multiple" ? (
          <div
            css={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-around",
              padding: "2rem",
            }}
          >
            <div
              css={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-around",
                padding: "1rem",
              }}
            >
              <div
                css={{
                  margin: "1rem",
                  width: "7rem",
                  display: "flex",
                  justifyContent: "flex-start",
                }}
              >
                <input
                  type="radio"
                  name="answer"
                  value={answers[0]}
                  checked={selectedAnswer === answers[0]}
                  onChange={handleOnChange}
                />
                <span css={{ margin: "0 0 0 1rem" }}>{answers[0]}</span>
              </div>
              <div
                css={{
                  margin: "1rem",
                  width: "7rem",
                  display: "flex",
                  justifyContent: "flex-start",
                }}
              >
                <input
                  type="radio"
                  name="answer"
                  value={answers[1]}
                  checked={selectedAnswer === answers[1]}
                  onChange={handleOnChange}
                />
                <span css={{ marginLeft: "1rem" }}>{answers[1]}</span>
              </div>
            </div>
            <div
              css={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-around",
                padding: "1rem",
              }}
            >
              <div
                css={{
                  margin: "1rem",
                  width: "7rem",
                  display: "flex",
                  justifyContent: "flex-start",
                }}
              >
                <input
                  type="radio"
                  name="answer"
                  value={answers[2]}
                  checked={selectedAnswer === answers[2]}
                  onChange={handleOnChange}
                />
                <span css={{ marginLeft: "1rem" }}>{answers[2]}</span>
              </div>
              <div
                css={{
                  margin: "1rem",
                  width: "7rem",
                  display: "flex",
                  justifyContent: "flex-start",
                }}
              >
                <input
                  type="radio"
                  name="answer"
                  value={answers[3]}
                  checked={selectedAnswer === answers[3]}
                  onChange={handleOnChange}
                />
                <span css={{ marginLeft: "1rem" }}>{answers[3]}</span>
              </div>
            </div>
          </div>
        ) : (
          <div
            css={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-around",
              padding: "1rem",
            }}
          >
            <div
              css={{
                margin: "1rem",
                width: "7rem",
                display: "flex",
                justifyContent: "flex-start",
              }}
            >
              <input
                type="radio"
                name="answer"
                value={answers[0]}
                checked={selectedAnswer === answers[0]}
                onChange={handleOnChange}
              />
              <span css={{ marginLeft: "1rem" }}>{answers[0]}</span>
            </div>
            <div
              css={{
                margin: "1rem",
                width: "7rem",
                display: "flex",
                justifyContent: "flex-start",
              }}
            >
              <input
                type="radio"
                name="answer"
                value={answers[1]}
                checked={selectedAnswer === answers[1]}
                onChange={handleOnChange}
              />
              <span css={{ marginLeft: "1rem" }}>{answers[1]}</span>
            </div>
          </div>
        )}
      </div>
      <button
        onClick={() => {
          setAnswerBlock(true);
          const isCorrectAnswer =
            selectedAnswer === questionData.correct_answer;

          isCorrectAnswer
            ? alert("Correct Answer!")
            : alert("Incorrect Answer!");

          setTimeout(() => {
            nextQuestion(isCorrectAnswer);
            setAnswerBlock(false);
          }, 800);
        }}
      >
        Next Question
      </button>
    </div>
  );
};
