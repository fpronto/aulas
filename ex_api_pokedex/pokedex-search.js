const apiUrl = "https://pokeapi.co/api/v2/pokemon/";

function getPoke() {
  const textSearch = document.getElementById("search").value;
  fetch(apiUrl + textSearch.toLowerCase())
    .then((response) => response.json())
    .then((data) => {
      document.getElementById("ID").innerHTML = data.id;
      document.getElementById("Name").innerHTML = data.name;
      document.getElementById("Type").innerHTML = data.types
        .map((data) => data.type.name)
        .join(" | ");
      document.getElementById("Picture").src =
        data.sprites.other["official-artwork"].front_default;
    });
}

// Part 2
// Iteradores + Create Element
