const apiUrl = "https://pokeapi.co/api/v2/pokemon/";

const totalNumber = Array.from(Array(1008).keys());

for (let index = 0; index <= 1008; index++) {
  try {
    const response = await fetch(`${apiUrl}${index + 1}`);
    const data = await response.json();
    const template = `<p id="ID">${data.id}</p>
    <p id="Name">${data.name}</p>
    <img id="Picture" src="${
      data.sprites.other["official-artwork"].front_default
    }"/>
    <p id="Type">${data.types.map((t) => t.type.name).join(" | ")}</p>`;

    const divGroupPokemon = document.createElement("div");
    divGroupPokemon.innerHTML = template;

    document.getElementById("gallery").appendChild(divGroupPokemon);
    //totalNumber.forEach((num) => {
    /* alternative mode:
        // const divGroupPokemon = document.createElement("div");
        // const pId = document.createElement("p");
        // pId.innerHTML = data.id;
        // divGroupPokemon.appendChild(pId);*/
  } catch (error) {
    console.log("error.message -> ", error.message);
  }
}
