//  Cria um jogo de para adivinhar um numero entre 0 e 100
//    o utilizador deve tentar introduzir um número para tentar adivinhar
//    o programa deve dizer se o número é maior ou menor do que o que o utilizador introduziu
//    ou se o utilizador acertou o número
//    No final do jogo o utilizador deve dizer quantas tentativas teve para acertar o número
//  Usar prompt-sync para entrada de dados
//    ? Bonus: Cria uma leaderboard com os 10 melhores jogos (fica em primeiro a pessoa que teve menos tentativas)
//
//  Tools:
//    1. Number.isNaN(inputNumber)
//    2. console.clear() || console.log("\x1B")
//    3. writeFileSync, readFileSync
