// Cria um script que recebe um numero e faz o sumatório de todos os numeros desde um até esse numero.
// O numero de input deve ser sempre maior do que 0
// Exemplo1:
//    Input: 1
//    Output: 1
// Exemplo2:
//    Input: 6
//    Output: 21 (1+2+3+4+5+6)
// Exemplo3:
//    Input: 10
//    Output: 55 (1+2+3+4+5+6+7+8+9+10)
// Usar prompt-sync para entrada de dados

const PromptSync = require("prompt-sync")()

let input = Number(PromptSync("Input your Number: "));
let output = 0

do {
    output = (input + output);
    input = input - 1;
} while (input > 0);

console.log(`This is the result of this operation: ${output}`);