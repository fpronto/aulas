// Cria uma função que recebe um numero e diz se é primo ou não.
// Usar prompt-sync para entrada de dados

const PromptSync = require("prompt-sync")();

let input = Number(PromptSync("Introduz o teu número:"));
let div = input - 1;
let res

if (input < 2) {
    console.log("Este não é um número primo.");
    process.exit(0)
}

do {
    res = (input % div);
    div -= 1;
    if (res === 0){
        break
    }
} while (div > 1);

if (res !== 0) {
    console.log("Este é um número primo.")
} else {
    console.log("Este não é um número primo.")
}