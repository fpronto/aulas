//  Cria um jogo de para adivinhar um numero entre 0 e 100
//    o utilizador deve tentar introduzir um número para tentar adivinhar
//    o programa deve dizer se o número é maior ou menor do que o que o utilizador introduziu
//    ou se o utilizador acertou o número
//    No final do jogo o utilizador deve dizer quantas tentativas teve para acertar o número
//  Usar prompt-sync para entrada de dados
//    ? Bonus: Cria uma leaderboard com os 10 melhores jogos (fica em primeiro a pessoa que teve menos tentativas)
//
//  Tools:
//    1. Number.isNaN(inputNumber)
//    2. console.clear() || console.log("\x1B")
//    3. writeFileSync, readFileSync

import PromptSync from "prompt-sync";
import { writeFileSync, readFileSync } from 'fs'

//const leaderBoard = [{nome: "francisco", try: 1}];
//writeFileSync("./leaderBoard.json", JSON.stringify(leaderBoard));

function generateARandomNumber(max: number, min: number): number {
    return Math.floor(Math.random() * (max - min) + min);
}

const prompt = PromptSync({ sigint: true });

let inputNumber: number = undefined
console.log("")
console.log("ADIVINHA O NÚMERO - Versão Beta 1.1")
console.log("")
console.log("O teu objectivo é adivinhar que número estou a pensar.")
console.log("Pista: É um número entre 0 e 100 (inclusivé)!")
console.log("Boa sorte!")

interface playerData {
    playerName: string;
    playerScore: number;
}

let leaderBoardDb: Array<playerData> = [];
const filename = "leaderboard.json"

function dbResync() {
    try {
        const leaderBoardRaw = readFileSync(filename, { encoding: "utf-8" })
        leaderBoardDb = JSON.parse(leaderBoardRaw)
        leaderBoardDb.sort((a, b) => a.playerScore - b.playerScore);
    } catch (err) { }
}

function saveScoreToLb(name: string, numTry: number) {
    dbResync()

    leaderBoardDb.push({ playerName: name, playerScore: numTry });
    leaderBoardDb.sort((a, b) => a.playerScore - b.playerScore);
    leaderBoardDb = leaderBoardDb.slice(0, 10);

    writeFileSync(filename, JSON.stringify(leaderBoardDb, undefined, "\t"))
}

function play() {
    let playAgain = undefined
    do {
        const numberToGuess: number = generateARandomNumber(100, 0)
        let numberOfTries: number = 0
        do {
            do {
                inputNumber = Number(prompt("Qual é o teu palpite? Introduz um número entre 0 e 100 (inclusivé): "));

                if (Number.isNaN(inputNumber) || inputNumber > 100 || inputNumber < 0) {
                    console.log("Entrada não é um número ou não está dentro do intervalo indicado. Tenta de novo.")
                }
            } while (Number.isNaN(inputNumber) || inputNumber > 100 || inputNumber < 0)

            if (inputNumber > numberToGuess) {
                console.log("O teu palpite está acima do número em jogo. Tenta de novo.")
            } else if (inputNumber < numberToGuess) {
                console.log("O teu palpite está abaixo do número em jogo. Tenta de novo.")
            } else {
                console.log("O teu palpite está certo! Parabéns!")
            }

            numberOfTries += 1

        } while (inputNumber !== numberToGuess);

        const tryCountText: string = numberOfTries === 1 ? "tentativa" : "tentativas"

        console.log(`Precisaste de ${numberOfTries} ${tryCountText} para acertar.`)
        console.log("")
        console.log("")
        let saveToLeaderBoard:string
        
        do {
            console.log("Queres guardar o teu score?")
            saveToLeaderBoard = prompt("S/N? ").toUpperCase()
            if (saveToLeaderBoard === "S") {
            const playerName = prompt("Escreve o teu nome para veres se entraste para a Leaderboard:").toUpperCase()
                saveScoreToLb(playerName, numberOfTries)
            }
        } while (saveToLeaderBoard !== "S" && saveToLeaderBoard !== "N");

        console.log("")
        console.log("")
        console.log("Queres voltar a jogar?")
        playAgain = prompt("S/N? ").toUpperCase()
    } while (playAgain === "S");
}

function displayLb(array: Array<playerData>) {
    console.log()
    for (let index = 0; index < array.length; index++) {
        const nameLb = array[index].playerName;
        const scoreLb = array[index].playerScore;
        console.log(`${index + 1}. ${nameLb}: ${scoreLb}`)
    }
}

function menu() {

    let menuOption: number = undefined
    do {
        console.log("")
        console.log("Indica a tua opção:")
        console.log("1. Jogar")
        console.log("2. LeaderBoard")
        console.log("0. Sair")
        console.log("")
        do {
            menuOption = Number(prompt("Opção: "));
            if (menuOption > 2 || menuOption < 0 || Number(isNaN(menuOption))) {
                console.log("Opção Inválida.")
            }
        } while (menuOption > 2 || menuOption < 0 || Number(isNaN(menuOption)))

        if (menuOption === 1) {
            play()
        } else if (menuOption === 2) {
            dbResync()
            displayLb(leaderBoardDb)
        }
    } while (menuOption !== 0);
    console.clear()

}

menu()

