// Cria um script que calcula o IMC de uma pessoa
// Formula para o calculo: peso / (altura * altura)
// Consoante o valor do IMC deve indicar a sua classificação:
// Abaixo de 18.5: abaixo do peso
// Entre 18.5 e 25: peso normal
// Entre 25 e 30: acima do peso
// Acima de 30: obeso
// Usar prompt-sync para entrada de dados

const PromptSync = require("prompt-sync")();

let weight = Number(PromptSync("Insert your weight (in Kg): "))
let height = Number(PromptSync("Insert your height (in m): "))

let imc = (weight / (height * height)).toFixed(2);

if (imc < 18.5) {
    console.log(`Your IMC (${imc}) is too low. Check with your doctor, urgently....`);
} else if (imc < 25) {
    console.log(`Your IMC (${imc}) is OK. Keep up with whatever you're doing!`);
} else if (imc < 30) {
    console.log(`Your IMC (${imc}) is high. Start doing some exercise and eat better, now!`);
} else {
    console.log(`Your IMC (${imc}) says that you're obese. Check with your doctor, urgently....`);
}
