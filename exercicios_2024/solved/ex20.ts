// Cria um programa que mostra a tabuada de um determinado numero.
// Input: 5
// Output:
// 5 X 1 : 5
// 5 X 2 : 10
// ...
// 5 X 10: 50
// o input pode ser um numero de 1 a 100
// tools:
// console.log("linha1\nlinha2\nlinha3");

import * as PromptSync from "prompt-sync";

const prompt = PromptSync();

const input: number = Number(prompt("Introduz o número para gerarmos a sua tabuada: "))
const tabuada: Array<string> = []

for (let index = 1; index <= 10; index++) {
    const result:number = input * index
    tabuada.push(`${input} X ${index} = ${result}`)
}

console.log(tabuada.join("\n"))