// Jogo da forca
//   Cria um jogo de forca, onde o utilizador vai tentar acertar numa palavra selecionada aleatóriamente
//   O programa vai dizer quantas letras tem a palavra e vai pedir para o utilizador acertar uma letra ou na palavra
//   O programa vai dizer se o utilizador acertou ou se errou
// Tools:
//   Usar prompt-sync para entrada de dados
//   usar o console.clear() para limpar o ecra
//     @andsfonseca/palavras-pt-br

//   o
//  /|\
//  / \

import { BRISPELL } from "@andsfonseca/palavras-pt-br";
import PromptSync from "prompt-sync";
const prompt = PromptSync({ sigint: true });

function getAllWords(limit: number = 0, removeAccents: boolean = false, includeCompounds: boolean = true, includeSpecialCharacters: boolean = false, includeProperNouns: boolean = false) {

    let aux: string[] = BRISPELL

    if (limit > 0) {
        aux = aux.filter(a => a.length === limit)
    }

    if (!includeProperNouns) {
        aux = aux.filter(a => a[0] === a[0].toLowerCase())
    }

    if (!includeSpecialCharacters) {
        aux = aux.map(a => a.replace(/[.,\s]/g, ''))
    }

    if (!includeCompounds) {
        aux = aux.filter(a => a.indexOf('-') === -1)
    }

    if (removeAccents) {
        aux = aux.map(a => a.normalize("NFD").replace(/[\u0300-\u036f]/g, ""))
    }

    return Array.from<string>(new Set(aux));
}

function getRandomWord(limit: number = 0, removeAccents: boolean = false, includeCompounds: boolean = true, includeSpecialCharacters: boolean = false, includeProperNouns: boolean = false) {
    let aux = getAllWords(limit, removeAccents, includeCompounds, includeSpecialCharacters, includeProperNouns)
    return aux[Math.floor(Math.random() * aux.length)];
}

const wordToGuess = getRandomWord(0, true)
console.log(wordToGuess)

let hiddenChars = wordToGuess.split("").map(function hideChar(char) { return "-" })

let livesLeft: number = 5
let hideIntro: boolean = false

console.log("Jogo da Forca")
console.log("")
console.log("Adivinha a palavra mistério.")

do {
    menu()
} while (hiddenChars.some((char) => { return char === "-" }) && livesLeft > 0)


function menu() {
    if (hideIntro) {
        console.clear()
    } else {
        hideIntro = true
    }
    console.log("")
    console.log("Palavra mistério:")
    console.log(hiddenChars.join(" "))
    console.log("")
    console.log(`Tens ${livesLeft} vidas disponíveis. Por cada tentativa errada perdes 1 vida. Se tentares acertar a palavra e falhares, perdes 2 vidas.`)
    console.log("")
    const hasCharsGuessed = hiddenChars.some((char) => { return char !== "-" })

    if (!hasCharsGuessed) {
        guessChar()
    } else {
        console.log("Já adivinhas-te pelo menos uma letra. Queres continuar a adivinhar mais letras ou tentar adivinhar a palavra mistério?")
        console.log("")
        console.log("1. Adivinhar nova letra.")
        console.log("2. Adivinhar palavra mistério.")
        let reply: number
        do {
            reply = Number(prompt("Opção: "))
            if (reply !== 1 && reply !== 2 && Number.isNaN(reply)) {
                console.log("Opção inválida.")
            }
        } while (reply !== 1 && reply !== 2 && Number.isNaN(reply));

        if (reply === 1) {
            console.log("")
            guessChar()
        } else {
            console.log("")
            guessWord()
        }
    }
}

function guessChar() {
    let newGuess: string = ""
    do {
        newGuess = prompt("Adivinha uma das letras da palavra mistério: ")

        if (newGuess.length > 1) {
            console.log("Número de caractéres inválido. O palpite tem de conter apenas um caracter.")
            console.log("")
        }

    } while (newGuess.length > 1);

    let hasGuessedChar: boolean = false

    wordToGuess.split("").forEach((char, index) => {
        if (char === newGuess) {
            hiddenChars.splice(index, 1, newGuess)
            hasGuessedChar = true
        }
    })

    if (!hasGuessedChar) {
        livesLeft -= 1
    }

    if (hiddenChars.every( char => { return char !== "-" })) {
        console.log("")
        console.log(`Parabéns, desvendas-te o mistério!!\n\nA palavra mistério era ${wordToGuess}.`)
        console.log("")
    }
}

function guessWord() {
    const guessWordTry: String = prompt("Escreve o teu palpite para a palavra mistério: ")

    if (guessWordTry === wordToGuess) {
        hiddenChars = guessWordTry.split("")
        console.log(`Parabéns, desvendas-te o mistério!! A palavra mistério era ${wordToGuess}.`)
        console.log("")
    } else {
        console.log("Palpite Errado!")
        livesLeft -= 2
    }
}

if (livesLeft <= 0) {
    console.log("")
    console.log("Perdeste as vidas todas... Tenta jogar de novo!")
}