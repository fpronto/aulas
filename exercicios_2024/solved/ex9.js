// Cria um script que recebe vários numeros e diz qual o maior
// V1 - Usando o método do...while até recebe o numero 0
// V2 - Pedindo a quantidade de numeros e depois usando um loop <for> para pedir os valores
// V3 - Igual ao V1 mas dizer apenas qual o menor
// V4 - Igual ao V2 mas dizer apenas qual o menor
// V5 - Usar o metodo loop mais confortavel e dizer qual o maior e menor
// Usar prompt-sync para entrada de dados

const PromptSync = require("prompt-sync")();

const args = process.argv.slice(2);

switch (Number(args[0])) {
  case 1:
    v1();
    break;
  case 2:
    v2();
    break;
  case 3:
    v3();
    break;
  case 4:
    v4();
    break;
  case 5:
    v5();
    break;
  default:
    console.log("Insert a function version from 1 to 5.");
    break;
}

function v1() {
  let maxNumber;
  let input;

  do {
    input = PromptSync("Write a number: ");
    if (input !== "") {
      input = Number(input);
      if (maxNumber === undefined) {
        maxNumber = input;
      } else if (input > maxNumber) {
        maxNumber = input;
      }
    }
  } while (input !== 0);
  console.log(`The biggest number you inserted was ${maxNumber}`);
  return;
}

//fazer com 5 numeros
function v2() {
  let input;
  let maxNumber;

  for (let index = 0; index < 5; index++) {
    input = PromptSync("Write a Number: ");
    if (input === "") {
      index--;
    } else {
      input = Number(input);
      if (maxNumber === undefined) {
        maxNumber = input;
      } else if (input > maxNumber) {
        maxNumber = input;
      }
    }
  }
  console.log(`The biggest number you inserted was ${maxNumber}`);
}

//igual a v1 mas menor numero
function v3() {
  let minNumber;
  let input;

  do {
    input = PromptSync("Write a number: ");
    if (input !== "") {
      input = Number(input);
      if (minNumber === undefined || input < minNumber) {
        minNumber = input;
      }
    }
  } while (input !== 0);
  console.log(`The smallest number you inserted was ${minNumber}.`);
  return;
}

function v4() {
  let input;
  let minNumber;
  let defineInputNumber;

  do {
    defineInputNumber = Number(
      PromptSync("Set how many numbers you want to compare: ")
    );
  } while (defineInputNumber <= 0 || Number.isNaN(defineInputNumber));

  for (let index = 0; index < defineInputNumber; index++) {
    input = PromptSync("Write a Number: ");
    if (input === "") {
      index--;
    } else {
      input = Number(input);
      if (minNumber === undefined || input < minNumber) {
        minNumber = input;
      }
    }
  }
  console.log(`The smallest number you inserted was ${minNumber}.`);
}

//escolher for loop ou do while, definir number de entradas e devolver maior e menor numero.
function v5() {
  let minNumber = 0;
  let maxNumber = 0;
  let defineInputNumber;
  let input;

  do {
    defineInputNumber = Number(
      PromptSync("Set how many number you'll want to compare: ")
    );
  } while (defineInputNumber <= 0 || Number.isNaN(defineInputNumber));

  // primeira iteração do loop passa para fora
  /*
    do {
      input = PromptSync("Write the next number you want to compare: ");
    } while (input === "" || Number.isNaN(Number(input)));
    minNumber = Number(input);
    maxNumber = Number(input);
  */
  for (let index = 0; index < defineInputNumber; index++) {
    input = PromptSync("Write the next number you want to compare: ");
    if (input === "") {
      index--;
    } else {
      input = Number(input);
      if (input > maxNumber || index === 0) {
        maxNumber = input;
      }
      if (input < minNumber || index === 0) {
        minNumber = input;
      }
    }
  }
  console.log(
    `The biggest number is ${maxNumber} while the smallest is ${minNumber}`
  );
}
