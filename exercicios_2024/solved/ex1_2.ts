// Cria uma função que recebe dois numeros e diz qual o maior.
// Se os dois forem iguais, dizer que eles sao iguais
// Usar prompt-sync para entrada de dados
//
// Number.isNaN(<number>)
import PromptSync from "prompt-sync";

const prompt = PromptSync({ sigint: true })

let firstNum: number
let secondNum: number

do {
    firstNum = Number(prompt("Insert first number to be compared: "))
    secondNum = Number(prompt("Insert second number to be compared: "))
}
while (Number.isNaN(firstNum) || Number.isNaN(secondNum))

if (firstNum > secondNum) {
    console.log("First Number is the biggest.")
} else if (firstNum < secondNum) {
    console.log("Second Number is the biggest.")
} else {
    console.log("The numbers are equal to each other.")
}
