// Cria um script que recebe o nome de uma pessoa (2 até 4 palavras) e faz a sua abreviação
// A abreviação deve ser o primeira letra do primeiro e a primeira letra do ultimo nome separados por um ponto.
// Exemplo1:
//    Input: João da Silva
//    Output: J. S.
// Exemplo2:
//    Input: João da Silva de Oliveira
//    Output: J. O.
// Exemplo3:
//    Input: João miranda
//    Output: J. M.
// Bonus: Se o nome só tiver uma palavra deve criar uma abreviatura de uma letra só:
// Exemplo4:
//    Input: João
//    Output: J.
// Usar prompt-sync para entrada de dados

// possible tools:
// const chartAt4 = inputName.charAt(4)
// const newArray = inputName.split(" ");
// const firstPosition = newArray[0];
// const sizeArray = newArray.length;
// upperCase

const PromptSync = require("prompt-sync")();
const inputName = PromptSync("Write the name you want to abreviate: ");
const inputWords = inputName.split(" ");
const firstName = inputWords[0]
const inputLength = inputWords.length
const familyName = inputWords[inputLength - 1]

const firstLetterFirstName = firstName.charAt(0)
const firstLetterFamilyName = familyName.charAt(0)

console.log(`${firstLetterFirstName}. ${firstLetterFamilyName}.`)