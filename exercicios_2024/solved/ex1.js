// Cria uma função que recebe dois numeros e diz qual o maior.
// Se os dois forem iguais, dizer que eles sao iguais
// Usar prompt-sync para entrada de dados

const prompt = require("prompt-sync")();

function compareNumbers() {
    
    console.log("\nInput below the numbers to be compared.\n");
    
    let inputOne = prompt("Please write the first input number for comparison: ");
    let inputTwo = prompt("Please write the second input number for comparison: ");
    
    try {
        inputOne = parseInt(inputOne);
    } catch (error) {
  console.log(
    'Input Error. Only numbers are allowed and decimals require "." and not ",". Restart the script.'
  );
  return;
}

try {
  inputTwo = parseInt(inputTwo);
} catch (error) {
  console.log(
    'Input Error. Only numbers are allowed and decimals require "." and not ",". Restart the script.'
  );
  return;
}

if (inputOne === inputTwo) {
  console.log("The two numbers are the same.");
} else if (inputOne < inputTwo) {
  console.log(`${inputTwo} is the biggest number`);
} else if (inputOne > inputTwo) {
  console.log(`${inputOne} is the biggest number`);
}
}

compareNumbers();