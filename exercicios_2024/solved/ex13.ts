// Cria uma função que recebe uma string e diz quantas vezes aparece cada letra.
// Usar prompt-sync para entrada de dados

import * as PromptSync from "prompt-sync";

const prompt = PromptSync({sigint:true});
const input: string = prompt("Insere a palavra para ser detalhada: ")

const uniqueArray = (array :Array<string>) => [...new Set(array)];

/* 
- Unique
- Contar numero de vezes
- Push para um array de um objecto por caracter unico
- Console.log
*/

const inputArray: Array<string> = input.toLocaleLowerCase().split("");
let uniqueChars: Array<string> = uniqueArray(inputArray);

for (let index = 0; index < uniqueChars.length; index++) {
    let charCount: number = 0
    for (let indexInput = 0; indexInput < inputArray.length; indexInput++) {
        if (uniqueChars[index] === inputArray[indexInput]) {
            charCount += 1
        }
    }
    console.log(`Esta palavra tem ${charCount} caractéres ${uniqueChars[index]}.`)
}