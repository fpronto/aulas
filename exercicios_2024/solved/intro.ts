const randomNumbers = [4, 11, 42, 14, 39];
// const filteredArray = randomNumbers.filter();

const findFunction = n => {  
    return n > 5;
  }

  const filteredArray = []

for (let i = 0; i < randomNumbers.length; i++) {
    const element = randomNumbers[i];

    const verification = findFunction(element);
    if(verification) {
        filteredArray.push(element);
    }
    
}

const array = [3,4,5];

let squaredArray = array.map(arrayElement => arrayElement*arrayElement)

console.log(squaredArray);