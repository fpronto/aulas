// Cria uma função que recebe vários numeros e quando receber o numero 0, calcula a média dos outros valores.
// Usar prompt-sync para entrada de dados

import PromptSync from "prompt-sync";

let prompt = PromptSync({sigint: true})

let inputNum: number
let numArray: Array<number> = []
// let numArray: number[]

while (inputNum !== 0) {
    do {
        inputNum = Number(prompt("Input numbers to this app. We'll tell you the average of the inserted numbers afterward: "))
    } while (Number.isNaN(inputNum));
    
    if (inputNum !== 0) {
        numArray.push(inputNum)
    }
}

let sumArray: number = 0

numArray.forEach( element => {
    sumArray += element
})

let average: number = sumArray / numArray.length

console.log(`The average of all your inserted numbers is: ${average}.`)