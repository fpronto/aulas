// Cria uma função que recebe vários numeros e quando receber o numero 0, calcula a média dos outros valores.
// Usar prompt-sync para entrada de dados

const prompt = require("prompt-sync")()

let inputs;
let sumInput = 0;
let numInputs = 0;

do {
    inputs = parseInt(prompt("Input your number: "))
    sumInput = sumInput + inputs;
    numInputs = numInputs + 1;
} while (inputs != 0);

let average = (sumInput / (numInputs - 1)).toFixed(2);

console.log(`The average of all numbers is: ${average}.`)

//redo as V2 using array to store prompts.