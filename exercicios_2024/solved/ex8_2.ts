// Cria um script que calcula o IMC de uma pessoa
// Formula para o calculo: peso / (altura * altura)
// Consoante o valor do IMC deve indicar a sua classificação:
// Abaixo de 18.5: abaixo do peso
// Entre 18.5 e 25: peso normal
// Entre 25 e 30: acima do peso
// Acima de 30: obeso
// Usar prompt-sync para entrada de dados

import PromptSync from "prompt-sync";

const prompt = PromptSync({ sigint: true })
let weight: number = 0
let height: number = 0

do {
    weight = Number(prompt("Insert your Weight (in kilograms): "))
    if (Number.isNaN(weight)) {
        console.log("The input has to be a number, exclusively.");
    } else if (weight <= 0) {
        console.log("The input has to be bigger than 0.")
    }
} while (Number.isNaN(weight) || weight <= 0);

do {
    height = Number(prompt("Insert your Weight (in meters): "))
    if (Number.isNaN(height)) {
        console.log("The input has to be a number, exclusively.")
    } else if (height <= 0) {
        console.log("The input has to be bigger than 0.")
    }
} while (Number.isNaN(height) || height <= 0);

const imc: number = weight / (height * height)

console.log(`Your BMI coeficient is ${imc.toFixed(2)}`)

switch (true) {
    case imc < 18.5:
        console.log("You are underweight.")
        break;
    case imc < 25:
        console.log("Your weight is normal.")
        break;
    case imc < 30:
        console.log("You are overweight.")
        break;
    case imc > 30:
        console.log("You are obese. Stop eating now.")
        break;
}