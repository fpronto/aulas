// Cria uma função que recebe um numero e diz se é par ou impar.
// Usar prompt-sync para entrada de dados

import PromptSync from "prompt-sync";

const prompt = PromptSync({sigint: true})

let numberEntry: number

do {
    numberEntry = Number(prompt("Insert a number. We'll tell you if it's even or odd: "))
} while (Number.isNaN(numberEntry));

numberEntry%2 === 0 ? console.log("Your number is even.") : console.log("Your number is odd.")