// Cria uma função que recebe uma string e remove todos os espaços em branco
// Usar prompt-sync para entrada de dados
// Exemplo:
// Input: Olá eu sou o João e tenho 20 anos
// Output: OláeusouoJoãoetenho20anos

const PromptSync = require("prompt-sync")();
let inputSentence: string = PromptSync("Write the sentence to be trimmed:")

function trim (input: string) {
    let inputWords: Array<string> = input.split(" ")
    let trimmedSentence: string = inputWords.join("")
    console.log(trimmedSentence)
}

trim(inputSentence)