// Cria um programa para descodificar código morse
// 1. Começa por criar uma função isolada que recebe uma "letra" em código morse e devolve a letra correspondente descodificada
// 2. Recebendo uma determinada "frase" usa a função anterior e descodifica todas as letras da mesma (todas as letras são separadas por um espaço)
// Nota: os espaços entre palavras são representados "/"
// Letra	Morse
// A	.-   
// B	-... 
// C	-.-. 
// D	-..  
// E	.    
// F	..-. 
// G	--.  
// H	.... 
// I	..    
// J	.--- 
// K	-.-    
// L	.-..    
// M	--     
// N	-.     
// O	---    
// P	.--.    
// Q	--.-    
// R	.-.    
// S	...    
// T	-      
// U	..-     
// V	...-     
// W	.--    
// X	-..-    
// Y	-.--     
// Z	--..     
// 0	-----
// 1	.----
// 2	..---
// 3	...--
// 4	....-
// 5	.....
// 6	-....
// 7	--...
// 8	---..
// 9	----.

// Bonus: Cria um programa para fazer o inverso do que foi pedido anteriormente (dada uma string, cria o equivalente em morse)
// O objectivo é ler o ficheiro "morse.json" e descodificar o seu conteudo
// tools:
// importar todos as funções do fileSystem:
//      - import * as fs from 'node:fs'
//      - import * as fs from 'node:fs/promises'
// fs.readFile("path_to_text_file")
// const newJson = JSON.parse(text)
import * as PromptSync from "prompt-sync";

const prompt = PromptSync();
const input: string = prompt("Escreve o número da frase que queres traduzir: ")

if (Number(input) > 7 && Number(input) <= 0 ) {
    throw new Error("Input tem de ser entre 1 e 7")
}

import { readFileSync } from 'node:fs'

interface morseObj {
    phrase1: string,
    phrase2: string,
    phrase3: string,
    phrase4: string,
    phrase5: string,
    phrase6: string,
    phrase7: string,
}

function demorser(morseCharacter: string): string {

    const obj = {
        ".-": "A",
        "-...": "B",
        "-.-.": "C",
        "-..": "D",
        ".": "E",
        "..-.": "F",
        "--.": "G",
        "....": "H",
        "..": "I",
        ".---": "J",
        "-.-": "K",
        ".-..": "L",
        "--": "M",
        "-.": "N",
        "---": "O",
        ".--.": "P",
        "--.-": "Q",
        ".-.": "R",
        "...": "S",
        "-": "T",
        "..-": "U",
        "...-": "V",
        ".--": "W",
        "-..-": "X",
        "-.--": "Y",
        "--..": "Z",
        "-----": "0",
        ".----": "1",
        "..---": "2",
        "...--": "3",
        "....-": "4",
        ".....": "5",
        "-....": "6",
        "--...": "7",
        "---..": "8",
        "----.": "9",
        "/": " ",
    }

    const asciiChar = obj[morseCharacter];

    if (asciiChar == undefined) {
        throw new Error("Código morse introduzido não tem correspondência, revê e tenta de novo.")
    }

    return asciiChar
}

const currentPath = process.cwd();
const morseToDemorseRaw: string = readFileSync(`${currentPath}/morse.json`, { encoding: 'utf-8' })

let morseToDemorse: morseObj;
try {
    morseToDemorse = JSON.parse(morseToDemorseRaw);
} catch (err) {
    console.log("err parsing JSON", err);
}


function stringDemorser(stringMorse: string): string {
    const arrayMorse: Array<string> = stringMorse.split(" ")
    let asciiResult: string = ""
    for (let index = 0; index < arrayMorse.length; index++) {
        asciiResult += demorser(arrayMorse[index])
    }
    return asciiResult
}

console.log(stringDemorser(morseToDemorse[`phrase${input}`]))
