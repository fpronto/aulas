// Cria um script que recebe um ano e deve dizer o seculo desse ano
// Usar prompt-sync para entrada de dados
//tools:
// Math.floor(5.2) -> 5
// 1100 % 100 = 0 resto - mod (principalmente usado para validar se é par ou impar);
// 1100 / 100 = 11;

import * as PromptSync from "prompt-sync";
const prompt = PromptSync();

let yearToCheck: number = Number(prompt("Indica qual o ano para converter em século: "))
let output: number = yearToCheck / 100 
//passando calculo para o momento da declaração da variavel permitiu cortar o passo de mudança de valor da variavel dentro do if-else.


/*ANTES:

if ((yearToCheck % 100) === 0 ) {
    output = yearToCheck/100
    console.log(`O ano pertence ao século ${output}.`)
} else {
    output = Math.floor((yearToCheck/100))+1
    console.log(`O ano pertence ao século ${output}.`)
}
*/

//Optimização:

if ((yearToCheck % 100) !== 0) {
    output = Math.floor(output) + 1
} 
//else deixou de ser necessário, invertendo a condição, tendo em conta que nas situações em que o yearToCheck%100 tem valor === 0, o valor do output não precisa de ser mudado.

console.log(`O ano pertence ao século ${output}.`) 
//tendo em conta que o valor do output já está definido inicialmente, o console.log pode passar para fora do if.
