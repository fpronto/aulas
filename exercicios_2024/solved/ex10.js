// Cria uma função que recebe uma string e devolve o seu inverso
// Bonus: Se a string for um palindromo deve dizer que ele é um palindromo
// Usar prompt-sync para entrada de dados

const PromptSync = require("prompt-sync")();

const inputWord = PromptSync("Input the word to be reversed here: ");

function toReserve(input) {
    const arrayWord = [...input.split("")];
    arrayWord.reverse()
    return arrayWord.join("")
}

const outputWord = toReserve(inputWord)

if (inputWord.toLowerCase() === outputWord.toLowerCase()) {
    console.log("Your word is a palindrome... You trickester....")
} else {
    console.log(`The reverse of your word is: ${outputWord}`)
}

