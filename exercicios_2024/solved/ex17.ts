// Usando a funçao de gerar numeros aleatórios das utils, criar um script que:
// 1. Cria uma função que recebe este mesmo array e diz quantos elementos do array são pares
// 2. Cria uma função que recebe este mesmo array e diz quantos elementos do array estão abaixo de um determinado do numero 5
// Bonus:
// Dizer a quantidade de numeros que não cumprem o requisito
// usar a função generateUniqueValuesArray para gerar o array

import { generateValueArray } from "./utils";

/*let inputArray: Array<number> = generateUniqueValuesArray(6,0,10)
console.log(inputArray)
let evenCount: number = 0

for (let index = 0; index < inputArray.length; index++) {
    const indexMod: number = (inputArray[index]) % 2;
    if (indexMod === 0 && inputArray[index] != 0) {
        evenCount++
    }
}
console.log(`There are ${evenCount} even numbers on the array above`);*/

let inputArray: Array<number> = generateValueArray(10, 0, 20)
console.log(inputArray)
let negativeCount: number = 0
let over15Count: number = 0

for (let index = 0; index < inputArray.length; index++) {
    if (inputArray[index] < 10) {
        negativeCount++
    }
    if (inputArray[index] > 15) {
        over15Count++
    }

}

// const validation = null === true ? "Value if true": "Value if false";

let successValidation: string = negativeCount >= 4 ? "didn't go through" : "go through";

console.log(`This student got a grade under 10 on ${negativeCount} classes. On the other hand, the student got a grade over 15 on ${over15Count} classes. Considering these grades, the student ${successValidation} to next year.`);