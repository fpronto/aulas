// Cria duas funções
//  1. Cria uma função que recebe um ano e indica se é bisexto ou não (true ou false)
//  1.1 um ano bisexto é divisivel por (4 e não por 100) ou por 400
//  2. Cria uma função que recebe um mês e ano e indica o numero de dias do mês
// Usar prompt-sync para entrada de dados

import * as PromptSync from "prompt-sync";

const prompt = PromptSync({sigint:true});
const inputYear: number = Number(prompt("Escreve o ano que pretendes analisar: "))
const inputMonth: number = Number(prompt("Escreve o mês que pretendes analisar: "))

if (Number.isNaN(inputYear) || Number.isNaN(inputMonth)) {
    throw new Error(("Usaste caractéres inválidos no teu input, que deve conter apenas números, burro...").toUpperCase())
}

function isLeapYear(year: number):boolean {
    if (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)) {
        console.log ("O ano indicado é bisexto.")
        return true;
    } else {
        console.log ("O ano indicado não é bisexto.")
        return false;
    }
}

function numberOfDays(year:number, month:number):number {
    const leapYear = isLeapYear(year)
    
    switch (month) {
        case 2:
            if (leapYear === true) {
                return 29;
            } else {
                return 28;
            }
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return 31;
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
        default:
            throw new Error("O mês indicado não existe.");
    }
}

console.log(`O mês indicado tem ${numberOfDays(inputYear, inputMonth)} dias.`)
