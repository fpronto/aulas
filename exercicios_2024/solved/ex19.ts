// O instagram pediu a nossa ajuda para melhorar o algoritmo que escreve a frase de quantas pessoas gostou de um post.
// Cria um script que recebe um array de nomes e diz quantas pessoas gostaram de um post
// Deves mostrar as duas primeiras pessoas que gostaram do post
// Posteriormente deves colocar a quantidade de pessoas que gostaram do post (menos as duas que foram mostrada anteriormente)
// De lembrar que se menos de duas pessoas gostaram do post, deve mostrar todos os nomes
// Se ninguem gostou do post, deve dizer que ninguem gostou
// Exemplo1:
// 4: João, Maria e 2 outras pessoas gostaram do post
// Exemplo2:
// 2: João e a Maria gostaram do post
// Exemplo3: 
// 0: Ninguem gostou do teu post
// Usar função ex19 das utils para entrada de dados

import { ex19 } from "./utils";

//gera array com strings de qty aleatória (0 a 10 nomes)
//array[0][1]
//array.length com condicional para mostrar length -2 se qty >2

let names: Array<string> = ex19()
console.log(names)

switch (names.length) {
    case 2:
        console.log(`${names.join(" e ")} gostaram do teu post`)
        break;
    case 1:
        console.log(`${names[0]} gostou do teu post`)
        break;
    case 0:        
        console.log(`Ninguém gostou do teu post`)
        break;
    default:
        let lengthMinus2 = (names.length) - 2
        console.log(`${names[0]}, ${names[1]} e ${lengthMinus2} outras pessoas gostaram do teu post.`)
        break;
}
