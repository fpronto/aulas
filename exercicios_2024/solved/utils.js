"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ex19 = exports.ex18 = exports.generateValueArray = exports.generateUniqueValuesArray = void 0;
var unique_names_generator_1 = require("unique-names-generator");
/**
 * Use this function to generate an array with unique values
 * @param {number} length length of the array
 * @param {number} min each value will be created using this as a minumum value
 * @param {number} max each value will be created using this as a maximum value
 * @returns {Array<number>}
 */
var generateUniqueValuesArray = function (length, min, max) {
    var array = [];
    for (var i = 0; i < length; i++) {
        var isInArray = true;
        var random = void 0;
        do {
            random = Math.floor(Math.random() * (max - min) + min);
            isInArray = array.indexOf(random) !== -1;
        } while (isInArray);
        array.push(random);
    }
    return array;
};
exports.generateUniqueValuesArray = generateUniqueValuesArray;
/**
 * Use this function to generate an array with values
 * @param {number} length length of the array
 * @param {number} min each value will be created using this as a minumum value
 * @param {number} max each value will be created using this as a maximum value
 * @returns {Array<number>}
 */
var generateValueArray = function (length, min, max) {
    var array = [];
    for (var i = 0; i < length; i++) {
        var random = void 0;
        random = Math.floor(Math.random() * (max - min) + min);
        array.push(random);
    }
    return array;
};
exports.generateValueArray = generateValueArray;
var ex18 = function () {
    return (0, exports.generateUniqueValuesArray)(3, 1, 100);
};
exports.ex18 = ex18;
var generateUniqueNamesArray = function (minLength, maxLength) {
    var length = Math.floor(Math.random() * (maxLength - minLength) + minLength);
    var config = {
        dictionaries: [unique_names_generator_1.names],
    };
    var array = Array.from({ length: length }).map(function (_, _index, array) {
        var isInArray = true;
        var random;
        do {
            random = (0, unique_names_generator_1.uniqueNamesGenerator)(config);
            isInArray = array.indexOf(random) !== -1;
        } while (isInArray);
        return random;
    }, []);
    return array;
};
var ex19 = function () {
    return generateUniqueNamesArray(0, 10);
};
exports.ex19 = ex19;
