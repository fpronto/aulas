// Mira vai organizar um torneio de ténis. Os vários participantes já se inscreveram e apenas deram o seu nome, idade, sexo e indicaram se são federados ou não.
// Cria um script que irá indicar qual o escalão em que o participante vai participar.
// Os escalões serão os seguintes:
//    1. Federados séniores (femininos) (Idades: 23 a 35)
//    2. Federados séniores (masculinos) (Idades: 23 a 35)
//    3. Federados juvenis (femininos) (Idades: 17 a 22)
//    4. Federados juvenis (masculinos) (Idades: 17 a 22)
//    5. Séniores (femininos) (Idades: 23 a 35)
//    6. Sêniores (masculinos) (Idades: 23 a 35)
//    7. Juvenis (femininos) (Idades: 17 a 22)
//    8. Juvenis (masculinos) (Idades: 17 a 22)
//    9. Veteranos (femininos) (Idades: acima de 35)
//    10. Veteranos (masculinos) (Idades: acima de 35)
//    11. Juniores (femininos) (Idades: 13 a 16)
//    12. Juniores (masculinos) (Idades: 13 a 16)
//    13. Infantis (não há separação de genero) (Idades: inferior a 13 anos)
// No final deve fazer um relatório os vários jogadores por escalão
// Usar tenis.json para a entrada de dados

import tenisData from "./tenis.json";

for (let index = 0; index < tenisData.length; index++) {
    const element = tenisData[index]
    if (element.professional) {
        if (element.age < 23) {
            console.log(`O participante ${element.name} pertence ao escação Federados Juvenis ${element.gender === "male" ? "Masculino" : "Feminino"}.`)
        } else {
            console.log(`O participante ${element.name} pertence ao escação Federados Séniores ${element.gender === "male" ? "Masculino" : "Feminino"}.`)
        }
    } else {
        if (element.age < 13) {
            console.log(`O participante ${element.name} pertence ao escação Infantis.`)
        } else if (element.age < 17) {
            console.log(`O participante ${element.name} pertence ao escação Júniores ${element.gender === "male" ? "Masculino" : "Feminino"}.`)
        } else if (element.age < 23) {
            console.log(`O participante ${element.name} pertence ao escação Juvenis ${element.gender === "male" ? "Masculino" : "Feminino"}.`)
        } else if (element.age < 35) {
            console.log(`O participante ${element.name} pertence ao escação Séniores ${element.gender === "male" ? "Masculino" : "Feminino"}.`)
        } else {
            console.log(`O participante ${element.name} pertence ao escação Veteranos ${element.gender === "male" ? "Masculino" : "Feminino"}.`)
        }
    }
}