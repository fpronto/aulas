// Cria uma função que recebe um numero e diz se é par ou impar.
// Usar prompt-sync para entrada de dados

const prompt = require("prompt-sync")();

let onlyInput = prompt("Insert number to be evaluated: ")
onlyInput = parseInt(onlyInput);

if (onlyInput % 2 === 0) {
    console.log("The input number is an even number.")
} else {
    console.log("The input number is an odd number.")
}