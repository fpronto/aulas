// Cria uma função que recebe uma string e duplica todas as vogais
// Usar prompt-sync para entrada de dados
// Exemplo:
// Input: Abracadabra
// Output: AAbraacaadaabraa
var PromptSync = require("prompt-sync")();
//.join para conversão de Array para String, indicando quando o caracter de união.
var input = PromptSync("Word to be 'double-vowelized': ");
var chars = input.split("");
var output = [];
for (var index = 0; index < chars.length; index++) {
    switch (chars[index].toLowerCase()) {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            output.push(chars[index], chars[index]);
            break;
        default:
            output.push(chars[index]);
            break;
    }
}
console.log(output.join(""));
