// Cria uma função que recebe uma string e duplica todas as vogais
// Usar prompt-sync para entrada de dados
// Exemplo:
// Input: Abracadabra
// Output: AAbraacaadaabraa

const PromptSync = require("prompt-sync")();
//.join para conversão de Array para String, indicando quando o caracter de união.

const input: string = PromptSync("Word to be 'double-vowelized': ");
const chars: Array<string> = input.split("")
const output: Array<string> = []

for (let index: number = 0; index < chars.length; index++) {

    switch (chars[index].toLowerCase()) {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            output.push(chars[index],chars[index])
            break;
        default:
            output.push(chars[index])
            break;
    }
}

console.log(output.join(""))