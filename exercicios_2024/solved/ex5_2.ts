// Cria um script que recebe um numero e faz o sumatório de todos os numeros desde um até esse numero.
// O numero de input deve ser sempre maior do que 0
// Exemplo1:
//    Input: 1
//    Output: 1
// Exemplo2:
//    Input: 6
//    Output: 21 (1+2+3+4+5+6)
// Exemplo3:
//    Input: 10
//    Output: 55 (1+2+3+4+5+6+7+8+9+10)
// Usar prompt-sync para entrada de dados

import PromptSync from "prompt-sync";

let prompt = PromptSync({sigint: true})
let inputNumber: number = 0
let sum: number = 0

do {
    inputNumber = Number(prompt("Give us a number. We'll calculate the sum of all numbers between 1 and your number: "))
} while (Number.isNaN(inputNumber));

for (let index = 1; index <= inputNumber ; index++) {
    sum += index    
}

console.log(`The sum of all numbers between 1 and your number is ${sum}`)