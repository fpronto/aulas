// Cria uma função que recebe um numero e diz se é primo ou não.
// Usar prompt-sync para entrada de dados

import PromptSync from "prompt-sync";

const prompt = PromptSync({ sigint: true })

let inputNum: number

do {
    inputNum = Number(prompt("Insert the input number. We'll tell you if it's a prime number: "))
} while (Number.isNaN(inputNum));

let primeCount: number = 1
let index: number = inputNum - 1

do {
    if (inputNum % index === 0) {
        primeCount++
    }
    index--
} while (primeCount <= 2 && index > 0);

primeCount === 2 ? console.log("Your number is prime.") : console.log("Your number is not prime.")