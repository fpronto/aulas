// Mira vai organizar um torneio de ténis. Os vários participantes já se inscreveram e apenas deram o seu nome, idade, sexo e indicaram se são federados ou não.
// Cria um script que irá indicar qual o escalão em que o participante vai participar.
// Os escalões serão os seguintes:
//    1. Federados séniores (femininos) (Idades: 23 a 35)
//    2. Federados séniores (masculinos) (Idades: 23 a 35)
//    3. Federados juvenis (femininos) (Idades: 17 a 22)
//    4. Federados juvenis (masculinos) (Idades: 17 a 22)
//    5. Séniores (femininos) (Idades: 23 a 35)
//    6. Sêniores (masculinos) (Idades: 23 a 35)
//    7. Juvenis (femininos) (Idades: 17 a 22)
//    8. Juvenis (masculinos) (Idades: 17 a 22)
//    9. Veteranos (femininos) (Idades: acima de 35)
//    10. Veteranos (masculinos) (Idades: acima de 35)
//    11. Juniores (femininos) (Idades: 13 a 16)
//    12. Juniores (masculinos) (Idades: 13 a 16)
//    13. Infantis (não há separação de genero) (Idades: inferior a 13 anos)
// No final deve fazer um relatório os vários jogadores por escalão
// Usar tenis.json para a entrada de dados
