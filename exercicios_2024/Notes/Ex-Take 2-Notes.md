## Import PromptSync in ESM (ECMA Script Modules (Modules for JavaScript)) mode

```javascript
import PromptSync from "prompt-sync";
```

## Validation on if number is a not a number

```javascript
//general syntax:
Number.isNaN(<number>)

//example of prompt request that will keep being triggered while at least one of the entries is Not a Number:
do{
    firstNum = Number(prompt("Insert first number to be compared: "))
    secondNum = Number(prompt("Insert second number to be compared: "))
}
while (Number.isNaN(firstNum) || Number.isNaN(secondNum))
```

## Validation on if a number is prime

```javascript
/* A prime number can be divided by itself and by 1. If it's possible to divide by an additional number (smaller than itself), it's no longer prime.
Examples of Prime numbers - 1, 2, 3, 5

Example on how to validate it using JS:
*/
let primeCount: number = 1
let index: number = inputNum - 1

do {
    if (inputNum % index === 0) {
        primeCount++
    }
    index--
} while (primeCount <= 2 && index > 0);

primeCount === 2 ? console.log("Your number is prime.") : console.log("Your number is not prime.")
```
