// Jogo da forca
//   Cria um jogo de forca, onde o utilizador vai tentar acertar numa palavra selecionada aleatóriamente
//   O programa vai dizer quantas letras tem a palavra e vai pedir para o utilizador acertar uma letra ou na palavra
//   O programa vai dizer se o utilizador acertou ou se errou
// Tools:
//   Usar prompt-sync para entrada de dados
//   usar o console.clear() para limpar o ecra
//     @andsfonseca/palavras-pt-br

//   o
//  /|\
//  / \

import { BRISPELL } from "@andsfonseca/palavras-pt-br";
import PromptSync from "prompt-sync";
const prompt = PromptSync({ sigint: true });

function getAllWords(
  limit: number = 0,
  removeAccents: boolean = false,
  includeCompounds: boolean = true,
  includeSpecialCharacters: boolean = false,
  includeProperNouns: boolean = false
) {
  let aux: string[] = BRISPELL;

  if (limit > 0) {
    aux = aux.filter((a) => a.length === limit);
  }

  if (!includeProperNouns) {
    aux = aux.filter((a) => a[0] === a[0].toLowerCase());
  }

  if (!includeSpecialCharacters) {
    aux = aux.map((a) => a.replace(/[.,\s]/g, ""));
  }

  if (!includeCompounds) {
    aux = aux.filter((a) => a.indexOf("-") === -1);
  }

  if (removeAccents) {
    aux = aux.map((a) => a.normalize("NFD").replace(/[\u0300-\u036f]/g, ""));
  }

  return Array.from<string>(new Set(aux));
}

function getRandomWord(
  limit: number = 0,
  removeAccents: boolean = false,
  includeCompounds: boolean = true,
  includeSpecialCharacters: boolean = false,
  includeProperNouns: boolean = false
) {
  let aux = getAllWords(
    limit,
    removeAccents,
    includeCompounds,
    includeSpecialCharacters,
    includeProperNouns
  );
  return aux[Math.floor(Math.random() * aux.length)];
}
