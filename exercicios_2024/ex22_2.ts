// o teclado de um computador tem um problema e não consegue fazer backspace para apagar letras.
// cria uma função que recebe uma string e devolve uma nova string com o texto corrigido.
// se uma string tiver o caracter # então o caracter anterior deve ser removido
// se existirem multiplos # varios caracteres devem ser removidos
// Exemplo1:
//    abc#de##
// Resultado:
//    ab
// Exemplo2:
//    ab#cd
// Resultado:
//    acd
// Exemplo3:
//    ab##c
// Resultado:
//    c
// a entrada de dados pode ser feita por Prompt ou diretamente como variavel
