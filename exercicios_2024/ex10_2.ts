// Cria uma função que recebe uma string e devolve o seu inverso
// Bonus: Se a string for um palindromo deve dizer que ele é um palindromo
// Usar prompt-sync para entrada de dados

import PromptSync from "prompt-sync";

const prompt = PromptSync({sigint: true})
let input: string = ""

do {
    input = prompt("Tell us the word you want us to reverse for you: ")
} while (input === "")

const reversedInput: string = input.split("").toReversed().join("")

    if (reversedInput.toLowerCase() === input.toLowerCase()) {
        console.log("You were trying to trick us... Your word is a palindrome.")
    } else {
        console.log(`The reverse of ${input} is ${reversedInput}`)
    }

