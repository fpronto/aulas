// Cria uma função que recebe uma string e remove todos os espaços em branco
// Usar prompt-sync para entrada de dados
// Exemplo:
// Input: Olá eu sou o João e tenho 20 anos
// Output: OláeusouoJoãoetenho20anos

import PromptSync from "prompt-sync";

const prompt = PromptSync({sigint: true})

const input: string = prompt("Insert a sentence for us to compress to one word: ").split(" ").join("")

console.log(`Your sentence compressed to one word is: ${input}.`)