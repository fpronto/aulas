// Cria um script que recebe vários numeros e diz qual o maior
// V1 - Usando o método do...while até recebe o numero 0
// V2 - Pedindo a quantidade de numeros e depois usando um loop <for> para pedir os valores
// V3 - Igual ao V1 mas dizer apenas qual o menor
// V4 - Igual ao V2 mas dizer apenas qual o menor
// V5 - Usar o metodo loop mais confortavel e dizer qual o maior e menor
// Usar prompt-sync para entrada de dados

import PromptSync from "prompt-sync";

const prompt = PromptSync({ sigint: true })

const args = process.argv.slice(2);

switch (Number(args[0])) {
  case 1:
    v1();
    break;
  case 2:
    v2();
    break;
  case 3:
    v3();
    break;
  case 4:
    v4();
    break;
  case 5:
    v5();
    break;
  default:
    console.log("Insert a function version from 1 to 5.");
    break;
}

//Do...while até receber stop.
function v1() {
  let input: number | string
  let biggestNum: number

  do {
    do {
      input = prompt("Insert numbers. We'll tell you the biggest number you gave us, until \"stop\" is inserted: ")
    } while (Number.isNaN(Number(input)) && input !== "stop");

    if (!Number.isNaN(Number(input))) {
      input = Number(input)
      if (input > biggestNum || biggestNum === undefined) {
        biggestNum = input
      }
    }
  } while (input !== "stop");

  return console.log(`The biggest number you inserted was ${biggestNum}`)

}

//fazer com 5 numeros
function v2() {
  let biggestNum: number
  let numberOfNumbers: number

  do {
  numberOfNumbers = Number(prompt("Tell us how many numbers you'll input, and at the end we'll tell you which one was the biggest: "))
  } while(Number.isNaN(numberOfNumbers))


  for (let index = 0; index < numberOfNumbers; index++) {
    let inputNum: number | string
    
    do {
      inputNum = prompt("Insert the next Number: ")
    } while (Number.isNaN(Number(inputNum)))

    if (Number(inputNum) > biggestNum || biggestNum === undefined) {
      biggestNum = Number(inputNum)
    }
  }

  console.log(`The bigest number you've input was ${biggestNum}.`)
}

//igual a v1 mas menor numero
function v3() {
  let input: number | string
  let smallestNum: number

  do {
    input = prompt(`Insert numbers. We'll tell you which was the smallest, until "stop" is inserted: `)
    
    if (Number(input) < smallestNum || (smallestNum === undefined && /^\d+$/g.test(input))) {
      smallestNum = Number(input)
    }
    
  } while (input.toLowerCase() !== "stop");

  console.log(`The smallest number you've given us was ${smallestNum}`)
}

//igual a v2 mas menor numero
function v4() {
  

  
 }

//
function v5() { }
