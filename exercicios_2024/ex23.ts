// Corrida de Berlindes
//    Cria um programa que simule uma corrida de Berlindes
//    A corrida vai ser de 10 metros
//    Antes da corrida começar, o programa vai pedir para cada berlinde:
//       - o nome
//       - a velocidade média (numero metros por segundo)
//    Assim que o programa tiver mais do que 2 Berlindes, o utilizador pode selecionar a opção de iniciar a corrida:
//       - ao iniciar a corrida o programa deve dizer qual delas chega primeiro à meta
//       - Bonus: o programa deve mostrar a leaderboard e os tempos de cada berlinde
//       - Bonus 2: Adicionar possiveis tecnicas de lançamento que possam aumentar ou diminuir a velocidade inicialmente (mas depois estabiliza)
//       - Bonus 3: Adicionar curvas e contra curvas com limites de velocidade (se o berlinde passar lá a essa velocidade, espeta-se)
//    O programa deve mostrar as opções ao utilizador e pedir para o utilizador escolher uma das opções
//    Exemplo:
//       Corrida de Berlindes
//       0 Berlindes na corrida
//       1. Adicionar berlinde
//       2. Iniciar corrida (minimo 2 berlindes)
//       3. Sair
//       Escolha uma opção: <prompt>
//    Se o utilizador selecionar a opção 2 e ainda não tiver berlindes adicionados, o programa deve pedir para o utilizador adicionar um berlinde, e depois voltar ao menu inicial

