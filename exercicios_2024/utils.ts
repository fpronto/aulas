import { uniqueNamesGenerator, Config, names } from "unique-names-generator";

/**
 * Use this function to generate an array with unique values
 * @param {number} length length of the array
 * @param {number} min each value will be created using this as a minumum value
 * @param {number} max each value will be created using this as a maximum value
 * @returns {Array<number>}
 */
export const generateUniqueValuesArray = (
  length: number,
  min: number,
  max: number
): number[] => {
  const array: number[] = [];

  for (let i = 0; i < length; i++) {
    let isInArray: boolean = true;
    let random: number;
    do {
      random = Math.floor(Math.random() * (max - min) + min);
      isInArray = array.indexOf(random) !== -1;
    } while (isInArray);
    array.push(random);
  }

  return array;
};

/**
 * Use this function to generate an array with values
 * @param {number} length length of the array
 * @param {number} min each value will be created using this as a minumum value
 * @param {number} max each value will be created using this as a maximum value
 * @returns {Array<number>}
 */
export const generateValueArray = (
  length: number,
  min: number,
  max: number
): number[] => {
  const array: number[] = [];

  for (let i = 0; i < length; i++) {
    let random: number;
      random = Math.floor(Math.random() * (max - min) + min);
    array.push(random);
  }

  return array;
};

export const ex18 = (): number[] => {
  return generateUniqueValuesArray(3, 1, 100);
};

const generateUniqueNamesArray = (
  minLength: number,
  maxLength: number
): string[] => {
  const length: number = Math.floor(
    Math.random() * (maxLength - minLength) + minLength
  );
  const config: Config = {
    dictionaries: [names],
  };

  const array: string[] = Array.from({ length }).map((_, _index, array) => {
    let isInArray: boolean = true;
    let random: string;
    do {
      random = uniqueNamesGenerator(config);
      isInArray = array.indexOf(random) !== -1;
    } while (isInArray);
    return random;
  }, []);
  return array;
};

export const ex19 = (): string[] => {
  return generateUniqueNamesArray(0, 10);
};
