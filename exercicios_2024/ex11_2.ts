// Cria uma função que recebe uma string e duplica todas as vogais
// Usar prompt-sync para entrada de dados
// Exemplo:
// Input: Abracadabra
// Output: AAbraacaadaabraa

import PromptSync from "prompt-sync";

const prompt = PromptSync({sigint: true})

let input: string = ""

do {
    input = prompt("Give us a word. We'll find the vowels and duplicate them: ")
} while (input === "");

const output: string = input.split("").map( (element) => {
    switch (element.toLowerCase()) {
        case "a":
        case "e":
        case "i":
        case "o":
        case "u":
            return element+element;
        default:
            return element;
}}).join("")

// This whole code above could have been coded with REGEX as shown below:
// const outputRegex = input.replace(/([aeiou])/gi, "$1$1")

console.log(`This is your word transformed: ${output}.`)