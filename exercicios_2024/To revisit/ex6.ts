/* Os algarismos romanos são representados por sete símbolos diferentes: I, V, X, L, C, D e M.

Valor do símbolo
I ->  1
V ->  5
X ->  10
L ->  50
C ->  100
D ->  500
M ->  1000

Por exemplo, 2 é escrito como II em algarismo romano, apenas duas unidades somadas. 12 é escrito como XII, que é simplesmente X + II. O número 27 é escrito como XXVII, que é XX + V + II.

Os algarismos romanos são geralmente escritos do maior para o menor, da esquerda para a direita. No entanto, o número quatro não é IIII. Em vez disso, o número quatro é escrito como IV.
Como o um está antes do cinco, subtraímo-lo, totalizando quatro. O mesmo princípio se aplica ao número nove, que é escrito como IX. Existem seis casos em que a subtração é usada:

I pode ser colocado antes de V (5) e X (10) para formar 4 e 9.
X pode ser colocado antes de L (50) e C (100) para formar 40 e 90.
C pode ser colocado antes de D (500) e M (1000) para perfazer 400 e 900.
Dado um algarismo romano, converta-o para um número inteiro.
*/

//TODO: Falta implementar a validação de dados e garantir que o numero romano introduzido vem na ordem de caracteres correta.

import * as PromptSync from "prompt-sync";

const prompt = PromptSync();
const input: string = prompt("Escreve o número romano que queres converter para numeração árabe:")
const toUpperCase: string = input.toUpperCase()
let count: number = 0
let outputNumber: number = 0


do {

    let multiplier: number = 1;
    const nextNumber = toUpperCase.charAt(count + 1)
    if (toUpperCase.charAt(count) == "I" && (nextNumber == "V" || nextNumber == "X")) {
        multiplier = -1;
    } else if (toUpperCase.charAt(count) == "X" && (nextNumber == "L" || nextNumber == "C")) {
        multiplier = -1;
    } else if (toUpperCase.charAt(count) == "C" && (nextNumber == "D" || nextNumber == "M")) {
        multiplier = -1;
    }

    switch (toUpperCase.charAt(count)) {
        case "I":
            outputNumber += 1 * multiplier
            break;
        case "V":
            outputNumber += 5
            break;
        case "X":
            outputNumber += 10 * multiplier
            break;
        case "L":
            outputNumber += 50
            break;
        case "C":
            outputNumber += 100 * multiplier
            break;
        case "D":
            outputNumber += 500
            break;
        case "M":
            outputNumber += 1000
            break;
        default:
            throw new Error(("Usaste caractéres inválidos no teu número romano, burro...").toUpperCase())
    }

    count++
} while (count < toUpperCase.length);

console.log(outputNumber);

