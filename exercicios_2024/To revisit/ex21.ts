// Cria um simulador de batalha que diz quem venceu o combate
// De seguida vou determinar o valor de cada personagem no campo de batalha
// do lado bom:
//    Hobbits: 1
//    Men: 2
//    Elves: 3
//    Dwarves: 3
//    Eagles: 4
//    Wizards: 10
//
// do lado mau:
//    Orcs: 1
//    Men: 2
//    Wargs: 2
//    Goblins: 2
//    Uruk Hai: 3
//    Trolls: 5
//    Wizards: 10
//
// não existe qualquer restrição para a entrada de dados, desde que seja possivel perceber quantas personagens vão a combate
// a entrada de dados pode ser feita quer diretamente como variavel, como pedido de prompt ou como entrada num json

// Parte 2:
//      Adiciona uma nova funcionalidade: Fazer uma simulação para diferentes campos de batalha
//          Cada personagem vai ter uma vantagem em determinadas situações (em altura [cima para baixo], em plano [ambos os lados], em vale [baixo para cima])
//          Cada personagem vai ter uma percentagem de falha por personagem em cada campo de batalha
//          Adiciona ainda a possibilidade de lançar criticos, independente das outras vantagens/desvantagens
import { readFileSync } from "node:fs";

const cwd: string = process.cwd();

interface warDataValues {
  Units: number;
  Power: number;
}
interface warDataType {
  LightSide: {
    Hobbits: warDataValues;
    Men: warDataValues;
    Elves: warDataValues;
    Dwarves: warDataValues;
    Eagles: warDataValues;
    Wizards: warDataValues;
  };
  DarkSide: {
    Orcs: warDataValues;
    Mercenary: warDataValues;
    Wargs: warDataValues;
    Goblins: warDataValues;
    Uruk_Hai: warDataValues;
    Trolls: warDataValues;
    Nazgul: warDataValues;
    Wizards: warDataValues;
    Sauron: warDataValues;
  };
}

const warDataRaw: string = readFileSync(`${cwd}/LOTR.json`, {
  encoding: "utf-8",
});
const warData: warDataType = JSON.parse(warDataRaw);

let lightSideTeamData = Object.values(warData.LightSide);
let darkSideTeamData = Object.values(warData.DarkSide);

function battlePower(teamData: Array<warDataValues>): number {
  let teamPower: number = 0;

  for (let index = 0; index < teamData.length; index++) {
    const unitPower = teamData[index].Units * teamData[index].Power;
    teamPower += unitPower;
  }

  return teamPower;
}

console.log(`Light Side Power: ${battlePower(lightSideTeamData)}`);
console.log(`Dark Side Power: ${battlePower(darkSideTeamData)}`);

// asdasd
// // TODO: 1) Adicionar propriedades para vantagem e desvantagem de terrenos, com a respectiva %.
// // Introduzir conceito de eficácia. Apresentar menu na linha de comandos para selecção de terreno.
