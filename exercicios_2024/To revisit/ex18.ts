// Cria um script que recebe um array de numeros gerado pela função das utils
// O array vai ter sempre 3 numeros e vão gerados aleatóriamente, entre 1 e 100
// Diz qual o index do numero do meio (meio do array)
// Exemplo:
// Input: [5,2,10]
// Output: 0
// Input: [1,3,2]
// Output: 2
// Input: [5,6,7]
// Output: 1

//tools:
// Math.max(inputArray) = MAX
// Math.min(inputArray) = MIN
// const middle = inputArray.findIndex((value) =>{ return <codition> })

import { ex18 } from "./utils";

let inputArray: Array<number> = ex18()
console.log(inputArray)

/*
1º posição é do meio
2ª Posição é a maior e 3ª é a menor
3ª Posição é a maior e 2ª é a menor

2º posição é a do meio:
1ª Posição é a maior e 3ª é a menor
3ª Posição é a maior e 1ª é a menor

3º posição é do meio:
1ª Posição é a maior e 2ª é a menor
2ª Posição é a maior e 1ª é a menor
*/

if ((inputArray[1] > inputArray[0] && inputArray[2] < inputArray[0]) || (inputArray[2] > inputArray[0] && inputArray[1] < inputArray[0])) {
    console.log("O index é 0.")
}

if ((inputArray[0] > inputArray[1] && inputArray[2] < inputArray[1]) || (inputArray[2] > inputArray[1] && inputArray[0] < inputArray[1])) {
    console.log("O index é 1.")
}

if ((inputArray[0] > inputArray[2] && inputArray[1] < inputArray[2]) || (inputArray[1] > inputArray[2] && inputArray[0] < inputArray[2])) {
    console.log("O index é 2.")
}

//TODO: refazer com Max e Min