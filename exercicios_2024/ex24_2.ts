// Cria um programa para descodificar código morse
// 1. Começa por criar uma função isolada que recebe uma "letra" em código morse e devolve a letra correspondente descodificada
// 2. Recebendo uma determinada "frase" usa a função anterior e descodifica todas as letras da mesma (todas as letras são separadas por um espaço)
// Nota: os espaços entre palavras são representados "/"
// Letra	Morse
// A	.-
// B	-...
// C	-.-.
// D	-..
// E	.
// F	..-.
// G	--.
// H	....
// I	..
// J	.---
// K	-.-
// L	.-..
// M	--
// N	-.
// O	---
// P	.--.
// Q	--.-
// R	.-.
// S	...
// T	-
// U	..-
// V	...-
// W	.--
// X	-..-
// Y	-.--
// Z	--..
// 0	-----
// 1	.----
// 2	..---
// 3	...--
// 4	....-
// 5	.....
// 6	-....
// 7	--...
// 8	---..
// 9	----.

// Bonus: Cria um programa para fazer o inverso do que foi pedido anteriormente (dada uma string, cria o equivalente em morse)
// O objectivo é ler o ficheiro "morse.json" e descodificar o seu conteudo
// tools:
// importar todos as funções do fileSystem:
//      - import * as fs from 'node:fs'
//      - import * as fs from 'node:fs/promises'
// fs.readFile("path_to_text_file")
// const newJson = JSON.parse(text)
