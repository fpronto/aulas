// O instagram pediu a nossa ajuda para melhorar o algoritmo que escreve a frase de quantas pessoas gostou de um post.
// Cria um script que recebe um array de nomes e diz quantas pessoas gostaram de um post
// Deves mostrar as duas primeiras pessoas que gostaram do post
// Posteriormente deves colocar a quantidade de pessoas que gostaram do post (menos as duas que foram mostrada anteriormente)
// De lembrar que se menos de duas pessoas gostaram do post, deve mostrar todos os nomes
// Se ninguem gostou do post, deve dizer que ninguem gostou
// Exemplo1:
// 4: João, Maria e 2 outras pessoas gostaram do post
// Exemplo2:
// 2: João e a Maria gostaram do post
// Exemplo3:
// 0: Ninguem gostou do teu post
// Usar função ex19 das utils para entrada de dados
