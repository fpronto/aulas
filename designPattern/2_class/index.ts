class User {
  firstName: string;
  lastName: string;
  #email: string;

  constructor(firstName: string, lastName: string, email: string) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.#email = email;
  }

  fullName(separator: string = " "): string {
    return `${this.firstName}${separator}${this.lastName}`;
  }

  get email(): string {
    return this.#email;
  }

  set email(email: string) {
    this.#email = email.replace("@doe.com", "@google.comn");
  }
}

const user1 = new User("John", "Doe", "john@doe.com");
console.log("user1", user1.email);
user1.email = "jane@doe.com";
console.log("user1", user1.email);

export default user1.fullName;

// const user2 = new User("Jane", "Doe", "jane@doe.com");
