class Book {
  title: string;
  author: string;
  isbn: string;
  constructor(title: string, author: string, isbn: string) {
    this.title = title;
    this.author = author;
    this.isbn = isbn;
  }
}

const isbnNumbers = new Set<string>();

const bookList = [];

const addBook = (
  title: string,
  author: string,
  isbn: string,
  availibility: boolean,
  sales: number
) => {
  const createdBook = createBook(title, author, isbn);
  const book = {
    ...createdBook,
    sales,
    availibility,
    isbn,
  };

  bookList.push(book);

  return book;
};

const createBook = (title: string, author: string, isbn: string) => {
  isbnNumbers.add(isbn);

  let book = bookList.find((book) => book.isbn === isbn);
  if (!book) {
    book = new Book(title, author, isbn);
  }
  return book;
};

addBook("Harry Potter", "JK Rowling", "AB123", false, 100);

addBook("Harry Potter", "JK Rowling", "AB123", true, 50);

addBook("To Kill a Mockingbird", "Harper Lee", "CD345", true, 10);

addBook("To Kill a Mockingbird", "Harper Lee", "CD345", false, 20);

addBook("The Great Gatsby", "F. Scott Fitzgerald", "EF567", false, 20);

console.log("Total amount of copies: ", bookList.length);

console.log("Total amount of books: ", isbnNumbers.size);
