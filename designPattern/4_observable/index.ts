import PromptSync from "prompt-sync";
import { Observable } from "./Observable";

const prompt = PromptSync({ sigint: true });

const main = () => {
  const observableObj = new Observable();
  const firstSubscription = (data: number) => {
    console.log("First Subscription");
    console.log("Data received: ", data);
  };
  const secondSubscription = (data: number) => {
    console.log("Second Subscription");
    console.log("Data received: ", data);
  };

  observableObj.subscribe(firstSubscription);
  observableObj.subscribe(secondSubscription);
  prompt("Press enter to increment");
  observableObj.increment();

  prompt("Press enter to increment");
  observableObj.increment();
  prompt("Press enter to unsubscribe");
  observableObj.unsubscribe(firstSubscription);
  prompt("Press enter to increment");
  observableObj.increment();
};

main();
