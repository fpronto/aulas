import Counter from "./globalCounter";
export class Observable {
  observers: Function[];
  internalCounter: typeof Counter;
  constructor() {
    this.internalCounter = Counter;
    this.observers = [];
  }

  subscribe(f: Function) {
    this.observers.push(f);
  }

  unsubscribe(f: Function) {
    this.observers = this.observers.filter((subscriber) => subscriber !== f);
  }

  notify(data: number) {
    this.observers.forEach((observer) => observer(data));
  }

  increment() {
    this.internalCounter.increment();
    this.notify(this.internalCounter.getCount());
  }
  decrement() {
    this.internalCounter.decrement();
    this.notify(this.internalCounter.getCount());
  }
}
