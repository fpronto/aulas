export class Command {
  execute: Function;
  constructor(execute: Function) {
    this.execute = execute;
  }
}

export function PlaceOrderCommand(order: string, id: string) {
  return new Command((orders: Order[]) => {
    orders.push({ id, order });

    console.log(`You have successfully ordered ${order} (${id})`);
  });
}

export function CancelOrderCommand(id: string) {
  return new Command((orders: Order[]) => {
    const foundIndex = orders.findIndex((order) => order.id === id);
    if (foundIndex !== -1) {
      orders.splice(foundIndex, 1);
      console.log(`You have canceled your order ${id}`);
    }
  });
}

export function TrackOrderCommand(id: string) {
  return new Command((orders: Order[]) => {
    const foundOrder = orders.find((order) => order.id === id);
    if (foundOrder) {
      console.log(`Your order ${id} is on the way.`);
    } else {
      console.log(`Your order ${id} is not found.`);
    }
  });
}

export function PrintOrdersCommand() {
  return new Command((orders: Order[]) => console.log("Your orders: ", orders));
}
