import {
  Command,
  PlaceOrderCommand,
  PrintOrdersCommand,
  CancelOrderCommand,
  TrackOrderCommand,
} from "./commands";

class OrderManager {
  orders: Order[];
  constructor() {
    this.orders = [];
  }

  execute(command: Command, ...args: any) {
    return command.execute(this.orders, ...args);
  }
}

const manager = new OrderManager();

manager.execute(PlaceOrderCommand("Pad Thai", "1234"));

manager.execute(PlaceOrderCommand("Ramen", "2345"));

manager.execute(PrintOrdersCommand());

manager.execute(TrackOrderCommand("1234"));

manager.execute(CancelOrderCommand("1234"));

manager.execute(PrintOrdersCommand());
