import { getCount as getCountEntry1 } from "./entry1";
import { getCount as getCountEntry2 } from "./entry2";

import Counter from "./globalCounter";

Counter.increment();
const entry1 = getCountEntry1();
Counter.increment();
Counter.increment();
const entry2 = getCountEntry2();
console.log(`Entry 1: ${entry1}\nEntry 2: ${entry2}`);
Counter.increment();
console.log("Singleton: ", Counter.getCount());
