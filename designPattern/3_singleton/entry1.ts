import Counter from "./globalCounter";

export const getCount = () => {
  console.log("get Count from Entry 1");
  Counter.increment();
  return Counter.getCount();
};
