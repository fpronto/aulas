import { ChatRoom, User } from "./ChatRoom_User";

const main = () => {
  const chatRoom = new ChatRoom();
  const user1 = new User("user1", chatRoom);
  const user2 = new User("user2", chatRoom);
  user1.send("Hello user2");
  user2.send("Hello user1");
};

main();
