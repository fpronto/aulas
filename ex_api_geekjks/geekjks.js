const apiUrl = "https://geek-jokes.sameerkumar.website/api?format=json";


function getJoke () {
  fetch(apiUrl)
    .then(response => response.json())
    .then((jokes) => {
      console.log(jokes);
      document.getElementById("joke").innerHTML = jokes.joke;
    })
    .catch((err) => {
      console.log(err);
    })
}

getJoke();