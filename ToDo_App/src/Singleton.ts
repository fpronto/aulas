let instance: State | null = null;
class State {
  #toDoList: Array<todoItem>;
  constructor() {
    if (instance) {
      throw new Error("You can only create one instance!");
    }
    instance = this;
    this.#toDoList = [];
  }

  addToList(input: string) {
    this.#toDoList.push({ name: input, isDone: false });
  }

  isDoneToggle(index: number) {
    this.#toDoList[index].isDone = !this.#toDoList[index].isDone;
  }

  get toDoList() {
    return this.#toDoList;
  }
}

const singletonState = Object.freeze(new State());

class Observable {
  observers: Function[];
  constructor() {
    this.observers = [];
  }

  subscribe(f: Function) {
    this.observers.push(f);
  }

  unsubscribe(f: Function) {
    this.observers = this.observers.filter((subscriber) => subscriber !== f);
  }

  notify(data: Array<todoItem>) {
    this.observers.forEach((observer) => observer(data));
  }

  addToListObs(input: string) {
    singletonState.addToList(input);
    this.notify(singletonState.toDoList);
  }
  isDoneToggleObs(index: number) {
    singletonState.isDoneToggle(index);
    this.notify(singletonState.toDoList);
  }
}

export default Observable;
