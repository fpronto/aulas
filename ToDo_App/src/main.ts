import Observable from "./Singleton";

const state = new Observable();

const teste = document.getElementById("Teste");

if (teste === null) {
  throw new Error("Teste Component Not Available");
}

const component = <HTMLInputElement | null>document.getElementById("input");

if (component === null) {
  throw new Error("Input Component Not Available");
}

teste.innerText = `Size: 0`;

const clickButton = document.getElementById("addButton");

// <input type="checkbox" id="scales" name="scales" checked />
//     <label for="scales">Scales</label>

const handleOnClickCheckbox = (_event:Event, index: number) => {
    console.log("index -> ", index);
    // console.log("event -> ", event);
    // console.log('e.target.checked -> ', event?.target?.checked);
    state.isDoneToggleObs(index)
  };


function renderList(list: Array<todoItem>) {
  if (teste === null) {
    throw new Error("Teste Component Not Available");
  }

  teste.innerText = `Size: ${list.length}`;

  const ulElement = document.getElementById("list");
  if (ulElement === null) {
    throw new Error("List Component Not Available");
  }
  
  ulElement.innerHTML = "";

  list.forEach((item, index) => {
    const idName: string = item.name + index;
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.id = idName;
    checkbox.name = idName;
    checkbox.checked = item.isDone
    checkbox.addEventListener("change", (event) => {
      handleOnClickCheckbox(event, index)
    });

    // text-decoration: line-through;

    const label = document.createElement("label");
    label.htmlFor = idName;
    label.innerText = item.name;
    label.style.textDecoration = item.isDone ? "line-through":"none"

    const li = document.createElement("li");
    li.appendChild(checkbox);
    li.appendChild(label);

    ulElement.appendChild(li)
  });
}

function handleClick() {
  if (component === null) {
    throw new Error("Input Component Not Available");
  }

  const input = component.value;

  if (input === "") {
    alert("There's no Text on your ToDo.");
    return;
  }
  state.addToListObs(input);

  component.value = "";
}

if (clickButton === null) {
  throw new Error("Teste Component Not Available");
}

clickButton.addEventListener("click", handleClick);
component.addEventListener("keypress", (event) => {
  if (event.key === "Enter") {
    handleClick();
  }
});

state.subscribe(renderList);
