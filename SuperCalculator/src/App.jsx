import { useState } from "react";
import "./App.css";

function App() {
  const [input, setInput] = useState("");
  const [firstInput, setFirstInput] = useState("");
  const [currentOp, setCurrentOp] = useState("");

  const handleOnClick = (newValue) => {
    setInput(`${input}${newValue}`);
  };

  const handleClear = () => {
    setInput("");
  };

  const handleChangeOp = (newValue) => {
    setCurrentOp(newValue);
    setFirstInput(input);
    setInput("");
  };

  const handleCalculate = () => {
    switch (currentOp) {
      case "+":
        setInput(String(Number(firstInput) + Number(input)));
        break;
      case "-":
        setInput(String(Number(firstInput) - Number(input)));
        break;
      case "*":
        setInput(String(Number(firstInput) * Number(input)));
        break;
      case "/":
        setInput(String(Number(firstInput) / Number(input)));
        break;
      default:
        setInput("Invalid Operation");
        break;
    }
  };

  return (
    <div className="appWrapper">
      <div className="App">
        <div className="input">{input}</div>
        <div className="row">
          <div className="div-button" onClick={() => handleClear()}>
            C
          </div>
          <div className="div-button">&#177;</div>
          <div className="div-button">%</div>
          <div className="div-button" onClick={() => handleChangeOp("/")}>
            ÷
          </div>
        </div>
        <div className="row">
          <div className="div-button" onClick={() => handleOnClick("7")}>
            7
          </div>
          <div className="div-button" onClick={() => handleOnClick("8")}>
            8
          </div>
          <div className="div-button" onClick={() => handleOnClick("9")}>
            9
          </div>
          <div className="div-button" onClick={() => handleChangeOp("*")}>
            x
          </div>
        </div>
        <div className="row">
          <div className="div-button" onClick={() => handleOnClick("4")}>
            4
          </div>
          <div className="div-button" onClick={() => handleOnClick("5")}>
            5
          </div>
          <div className="div-button" onClick={() => handleOnClick("6")}>
            6
          </div>
          <div className="div-button" onClick={() => handleChangeOp("-")}>
            -
          </div>
        </div>
        <div className="row">
          <div className="div-button" onClick={() => handleOnClick("1")}>
            1
          </div>
          <div className="div-button" onClick={() => handleOnClick("2")}>
            2
          </div>
          <div className="div-button" onClick={() => handleOnClick("3")}>
            3
          </div>
          <div className="div-button" onClick={() => handleChangeOp("+")}>
            +
          </div>
        </div>
        <div className="row">
          <div className="div-button double" onClick={() => handleOnClick("0")}>
            0
          </div>
          <div className="div-button" onClick={() => handleOnClick(",")}>
            ,
          </div>
          <div className="div-button" onClick={handleCalculate}>
            =
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
