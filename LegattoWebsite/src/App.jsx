import {
  Navbar,
  Footer,
  AboutUs,
  ContactUs,
  Carousel,
  Filling,
  Media,
} from "./Sections";
import { Socials } from "./Components";
import styles from "./App.module.css";

const App = () => {
  return (
    <div id="top" className={styles.app}>
      <Navbar />
      <Carousel />
      <AboutUs />
      <Media />
      <ContactUs />
      <Socials vertical footerHide anchor={"right"} />
      <Footer />
    </div>
  );
};

export default App;
