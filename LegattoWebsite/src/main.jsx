import * as React from 'react'
import { ChakraProvider, extendTheme } from '@chakra-ui/react'
import * as ReactDOM from 'react-dom/client'
import App from './App'
import "./main.css"

const color1 = document.body.style.getPropertyValue('--color1');
const color2 = document.body.style.getPropertyValue('--color2');
const color3 = document.body.style.getPropertyValue('--color3');
const color4 = document.body.style.getPropertyValue('--color4');
const color5 = document.body.style.getPropertyValue('--color5');
const color6 = document.body.style.getPropertyValue('--color6');

const colors = {
  brand: {
    50: color6,
    500: color5,
    900: color4,
  },
}

const theme = extendTheme({ colors })

const rootElement = document.getElementById('root')
ReactDOM.createRoot(rootElement).render(
  <React.StrictMode>
    <ChakraProvider theme={theme}>
      <App />
    </ChakraProvider>
  </React.StrictMode>,
)

