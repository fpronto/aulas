import React, { useState } from "react";
import styles from "./Socials.module.css";
import { Link } from "@chakra-ui/react";
import { ReactComponent as FacebookLogo } from "../../assets/facebook.svg";
import { ReactComponent as InstagramLogo } from "../../assets/instagram.svg";
import { ReactComponent as MailLogo } from "../../assets/mail.svg";
import { ReactComponent as SoundCloudLogo } from "../../assets/soundcloud.svg";
import { ReactComponent as YoutubeLogo } from "../../assets/youtube.svg";
import classnames from "classnames";

const socialsList = [
  { altText: "mail", Icon: MailLogo, link: "mailto:corolegatto@gmail.com" },
  {
    altText: "facebook",
    Icon: FacebookLogo,
    link: "https://www.facebook.com/corolegatto",
  },
  {
    altText: "instagram",
    Icon: InstagramLogo,
    link: "https://www.instagram.com/corolegatto/",
  },
  {
    altText: "soundcloud",
    Icon: SoundCloudLogo,
    link: "https://soundcloud.com/coro-legatto",
  },
  {
    altText: "youtube",
    Icon: YoutubeLogo,
    link: "https://www.youtube.com/@corolegatto8635",
  },
];

const SocialsComponent = ({ vertical, footerHide, anchor }) => {
  const [hide, setHide] = useState(false);
  if (footerHide) {
    document.addEventListener("scroll", function (e) {
      let footerHeight = document.getElementById("footer").clientHeight
      let documentHeight = document.body.scrollHeight;
      let currentScroll = window.scrollY + window.innerHeight;
      // When the user is [modifier]px from the bottom, fire the event.
      if (currentScroll + footerHeight <= documentHeight) {
        setHide(false);
      }
      else {
        setHide(true);
      }
    });
  }
  return (
    <ul
      className={classnames(
        styles.ul,
        { [styles.ulVertical]: vertical },
        { [styles.ulAnchorRight]: anchor === "right" },
        { [styles.ulHide]: hide }
      )}
    >
      {socialsList.map((social) => {
        const { Icon } = social;
        return (
          <li key={social.altText}>
            <Link aria-label={social.altText} href={social.link} isExternal>
              <Icon className={styles.icons} />
            </Link>
          </li>
        );
      })}
    </ul>
  );
};

export default SocialsComponent;
