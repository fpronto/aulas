import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Autoplay } from "swiper";

import styles from "./Carousel.module.css";
import "swiper/css";
import "swiper/css/pagination";

import Carousel1 from "../../assets/Carousel1.jpg";
import Carousel2 from "../../assets/Carousel2.jpg";
import Carousel3 from "../../assets/Carousel3.jpg";
import Carousel4 from "../../assets/Carousel4.jpg";

const Carousel = () => {
  return (
    <div className={styles.Swiper}>
      <Swiper
        modules={[Pagination, Autoplay]}
        centeredSlides
        centeredSlidesBounds
        loop={true}
        autoplay={{ delay: 4000, pauseOnMouseEnter: true }}
      >
        <SwiperSlide className={styles.swyperSlide}>
          <img src={Carousel1} className={styles.img} />
        </SwiperSlide>
        <SwiperSlide className={styles.swyperSlide}>
          <img src={Carousel2} className={styles.img} />
        </SwiperSlide>
        <SwiperSlide className={styles.swyperSlide}>
          <img src={Carousel3} className={styles.img} />
        </SwiperSlide>
        <SwiperSlide className={styles.swyperSlide}>
          <img src={Carousel4} className={styles.img} />
        </SwiperSlide>
      </Swiper>
    </div>
  );
};

export default Carousel;
