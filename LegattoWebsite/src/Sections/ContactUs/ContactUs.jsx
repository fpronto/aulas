import React from "react";
import styles from "./ContactUs.module.css";
import { Button } from "@chakra-ui/react";
import { ReactComponent as ContactUsLogo } from "../../assets/contactUs.svg";
import { InstagramEmbed } from 'react-social-media-embed';

const ContactUs = () => {
  return (
    <div id="contactUs" className={styles.wrapper}>
      <h2 className={styles.h2}>Get in touch with us</h2>
        <InstagramEmbed
          url="https://www.instagram.com/p/CgMPQz_swIm/"
        />
      <div className={styles.buttonWrapper}>
        <Button
          colorScheme="gray"
          size="lg"
          className="contactB"
          leftIcon={<ContactUsLogo />}
          onClick={() => {
            location.href = "mailto:corolegatto@gmail.com";
          }}
        >
          Email
        </Button>
      </div>
    </div>
  );
};

export default ContactUs;
