import React from "react";
import styles from "./AboutUs.module.css";
import { Text } from "@chakra-ui/react";

const AboutUs = () => {
  return (
    <div id="aboutUs" className={styles.AboutUs}>
      <Text fontSize={"xx-large"} className={styles.Text}>
        About Us
      </Text>
      <p>
        O Coro Legatto, dirigido por Ana Raquel Azeiteiro, foi fundado a 1 de
        outubro de 2015, como secção cultural da Casa do Povo de Mira.
      </p>
      <p>
        “Legatto”, que significa ligado ou unido, espera ser o elo entre a
        música escrita e quem o ouve, contribuindo, assim, para o enriquecimento
        da cultura musical de todos.
      </p>
      <p>
        Desde a sua estreia, com a organização dos “Dias da Música em Mira” -
        que continuam a ser da sua responsabilidade - conta também com inúmeros
        concertos, realizados em vários pontos do país e em Espanha.
      </p>
      <p>
        Na II e IV edição dos “Dias da Música em Mira”, recebeu o professor e
        maestro Gonçalo Lourenço como mentor da Masterclass de Direção Coral,
        tendo tido como coro piloto o Coro Legatto.
      </p>
      <p>
        Em outubro de 2017, foi classificado em 1.º lugar no I Concurso de Coros
        de Coimbra e, em 2018, ficou em 3.º lugar no FICA, onde ganhou ainda o
        Prémio Especial de melhor interpretação do compositor David Miguel, com
        a peça “Soneto do amor e da morte”.
      </p>
      <Text fontSize={"x-large"} className={styles.Text}>
        Our Conductress
      </Text>
      <p>
        Ana Raquel Azeiteiro iniciou a sua vida musical aos 7 anos quando
        ingressou na Filarmónica Ressurreição de Mira onde aprendeu clarinete.
        Estudou no Conservatório de Música Calouste Gulbenkian de Aveiro, até ao
        5º grau, terminando o 7º grau do Curso Suplementar de Formação Musical
        no Conservatório de Música de Coimbra. Na sua vila natal, criou em 2011
        o Grupo Coral Infantil da Associação Cultural e Desportiva da
        Lentisqueira, onde se manteve como Diretora Pedagógica e Artística até
        ao ano de 2014. Ingressou na União Músicos de Mira no ano de 2013 como
        membro do corpo artístico e professora de Formação Musical, Clarinete,
        Classe de Conjunto e Aulas de Berço, até 2018.
      </p>
      <p>  
        Em 2015 fez uma formação de Novas Abordagens à Prática e ao Ensino do Canto, pela UNAVE, com a formadora Filipa Lã e com a mesma professora participou na Masterclass de Canto, promovida pela Allegr’arte, em Mira. Participou também no “Curso Internacional de Música Vocal” da Orquestra das Beiras e U. Aveiro, na qualidade de maestro participante, com o professor Gonçalo Lourenço. Em 2017 terminou o Mestrado em Ensino de Música, variante de Formação Musical e Música de Conjunto, na ESART/IPCB, com um projeto coral na ACAPO Coimbra. Também neste ano, completou o 1º ciclo da Classe de Aperfeiçoamento de Direção Coral em Lisboa, com o maestro Gonçalo Lourenço, onde trabalhou com Paulo Vassalo Lourenço e Tara Casher. Em 2018, participou na Masterclass de Direção Coral promovida pela Academia Coral de Verão de Leiria, com o professor Paulo Lourenço. Atualmente exerce funções de direção artística do Coro Legatto, de Mira - o qual fundou em 2015 - e dirige o AdesbAcapella, um ensemble vocal da Barreira (Leiria) e o Grupo Coral de Tancos. No presente ano letivo faz parte da Direção Pedagógica da Sociedade Artística Musical dos Pousos, Leiria, na qual é docente de Formação Musical e Berço das Artes, desde 2016.
      </p>
    </div>
  );
};

export default AboutUs;
