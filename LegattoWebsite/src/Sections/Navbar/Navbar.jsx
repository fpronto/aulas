import React from "react";
import { ReactComponent as Logo } from "../../assets/logoNB.svg";
import styles from "./Navbar.module.css"
import { Link } from "@chakra-ui/react"


const Navbar = () => {
  return (
    <div id="top" className={styles.top}>
      <nav className={styles.nav}>
        <a href="#top">
          <Logo className={styles.Logo} />
        </a>
        <ul className={styles.ul}>
          <li><Link className={styles.link} href="#aboutUs">About Us</Link></li>
          <li><Link className={styles.link} href="#events">Events</Link></li>
          <li><Link className={styles.link} href="#media">Media</Link></li>
          <li><Link className={styles.link} href="#contactUs">Contact Us</Link></li>
        </ul>
      </nav>
    </div>
  );
};

export default Navbar;
