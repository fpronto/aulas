import React from "react";
import styles from "./Footer.module.css";
import { Socials } from "../../Components";

const Footer = () => {
  const year = new Date().getFullYear();
  return (
    <div className={styles.footer} id="footer">
      <div className={styles.leftFooter}>
        © {year} - Coro Legatto Official Website.
        <br />
        All Rights Reserved.
      </div>
      <div className={styles.rightFooter}>
        <Socials />
      </div>
    </div>
  );
};

export default Footer;
