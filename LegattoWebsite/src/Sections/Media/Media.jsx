import React from "react";
import styles from "./Media.module.css";
import ReactPlayer from "react-player";
import { Text } from "@chakra-ui/react";


const Media = () => {
  return (
    <div id="media" className={styles.Media}>
      <Text fontSize={"xx-large"} className={styles.Text}>
        Media
      </Text>

      <div className={styles.ReactPlayerWrapper}>
        <ReactPlayer className={styles.ReactPlayer}
          width="720px"
          height="600px"
          url="https://soundcloud.com/coro-legatto/sets/concerto-de-natal-2019"
        />
        <ReactPlayer className={styles.ReactPlayer}
          width="720px"
          height="400px"
          controls muted={false} volume={0.80}
          url="https://www.youtube.com/watch?v=ko1oWnhdX-I&list=PLuTarx__EflucJykmREkpYg_L9grebaA0"
        />
      </div>
    </div>
  );
};
export default Media;
//
//
