import { random } from "lodash";

let listArray: Array<choiceItem> = [];

const addToList = (_event?: Event) => {
  const input: HTMLInputElement | null = <HTMLInputElement | null>(document.getElementById("input"));
  if (input === null) {
    throw new Error("Can't Access Input Element");
  }

  const inputValue: string = input.value

  if (input.value === "") {
    alert("There's no text to be added. Please retry.")
    return
  }

  listArray.push({ name: inputValue, isPicked: false })

  input.value = ""

  renderList()
};

const input: HTMLInputElement | null = <HTMLInputElement | null>(document.getElementById("input"));
if (input === null) {
  throw new Error("Can't Access Input Element");
}

const button: HTMLElement | null = document.getElementById("addInput");
if (button === null) {
  throw new Error("Can't Access Button Element");
}

button.addEventListener("click", addToList);
input.addEventListener("keypress", (event) => {
  if (event.key === "Enter") {
    addToList()
  }
});

const renderList = () => {
  const list: HTMLElement | null = document.getElementById("listToSort")
  if (list === null) {
    throw new Error("Can't Access List Element");
  }

  list.innerHTML = ""

  listArray.forEach((element) => {
    const liElement = document.createElement("li")
    liElement.innerHTML = element.name
    liElement.style.textDecoration = element.isPicked ? "line-through" : "none"
    list.appendChild(liElement)
  })
}

let sortedTask = ""

const sortElement = () => {
  const sortedArray = listArray.map((element, index) => {
    return element.isPicked ? null : index
  }).filter((element) => element !== null)

  if (sortedArray.length === 0) {
    alert("There are no tasks to be picked. Add new tasks before retrying to pick a task again.")
    return
  }

  const randomNumber: number = random(sortedArray.length - 1)

  const sortedElement = listArray[sortedArray[randomNumber]]
  sortedElement.isPicked = true
  sortedTask = sortedElement.name
  renderList()
  renderSortedElement()
}

const nextTaskButton = document.getElementById("pickNextTask")
nextTaskButton?.addEventListener("click", sortElement)

const renderSortedElement = () => {
  const task: HTMLElement | null = document.getElementById("selectedTask")
  if (task === null) {
    throw new Error("Can't Access Task Element")
  }
  
  task.innerHTML = sortedTask
}

const pickedTask = document.getElementById("selectedTask")
pickedTask?.addEventListener("click", () => { sortedTask = "" ; renderSortedElement() })

/*
- 🟩 Get input.
- 🟩 Add new object to listArray with arguments(input, picked (default false)).
- 🟩 Sort/Pick Function uses the Array Length as argument to random function to select the list entry to be shown.
  - 🟩 At start of Sort/Pick Function add step to create secondary Array which contains just entries with picked value as False.
- 🟩 Change picked value to True and Show picked entry.

WE STOPPED AT -> We concluded the rendering of the list. The CSS for the concluded to be stricked-through is already implemented.
Next: Do not forget the brilliant late night idea from Chico, where instead of creating a temporary Array to deal with the unpicked entries, to deal with it as numbersIndex [0,1,2,4,7].
*/
