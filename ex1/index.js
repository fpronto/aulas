function ex1() {
  const raio = parseInt(document.getElementById("ex1_value").value, 10);
  let area, perimetro;
  /**
   Exercicio 1
   */
  perimetro = 3.14 * raio;
  area = 3.14 * (raio * raio);
  document.getElementById("ex1_result").innerHTML = `A Area é <b>${area.toFixed(
    2
  )}</b>, o perimetro é K <b>${perimetro.toFixed(2)}</b>`;
}

function ex2() {
  const largura = parseInt(document.getElementById("ex2_value_larg").value, 10);
  const altura = parseInt(document.getElementById("ex2_value_alt").value, 10);
  let area;
  /**
   Exercicio 2
   */
  area = largura * altura;
  document.getElementById(
    "ex2_result"
  ).innerHTML = `A Area do retangulo é <b><i>${area}</i></b>`;
}
function ex3() {
  const cateto1 = parseInt(document.getElementById("ex3_value_cat1").value, 10);
  const cateto2 = parseInt(document.getElementById("ex3_value_cat2").value, 10);
  let area, hipotenusa;
  /**
   Exercicio 3
   Math.sqrt()
   */
  hipotenusa = (cateto1*cateto1)+(cateto2*cateto2);
  hipotenusa = Math.sqrt(hipotenusa);
  area = (cateto1*cateto2)/2; 
  document.getElementById(
    "ex3_result"
  ).innerHTML = `A Hipotenusa do Triangulo =: <b>${hipotenusa}</b><br/>Area do Triangulo = <b>${area}</b>`;
}

function ex4() {
  const tempFar = parseInt(document.getElementById("ex4_value").value, 10);
  let tempCels;
  /**
   Exercicio 4
   */
  tempCels = ((tempFar-32)/(1.8));
  document.getElementById(
    "ex4_result"
  ).innerHTML = `A Temperatura em Celsius é: ${tempCels}`;
}

function ex5() {
  /**
   Exercicio 5
   console.log()
   */
  let A = 1,B = 2
  console.log(A,B)
  let C = A; A = B ; B = C;
  console.log(A,B)
}

function ex6() {
  const velocity = parseInt(
    document.getElementById("ex6_value_velocidade").value,
    10
  );
  const duration = parseInt(
    document.getElementById("ex6_value_duracao").value,
    10
  );

  /**
   Exercicio 6
   */
    let distance;
    let consumption;
    distance = (velocity*duration);
    consumption = ((distance*(6.5))/100);
    
  document.getElementById("ex6_result").innerHTML = `A quantidade de combustível consumido foi ${consumption}`;
}
