let stopGame = false;

function changePl() {
  let currentPl = document.getElementById("PlayerID").innerHTML;
  if (currentPl === "X") {
    currentPl = "O"
  } else {
    currentPl = "X"
  }
	document.getElementById("PlayerID").innerHTML = currentPl;
}

function handleClick(event) {
  console.log('event -> ', event);
	if (stopGame) { //validation on IF considers always that first behaviour is for true scenario and remaining for false scenario(s). Since it's a bolean, we can just call the variable.
		return;
  }
  // truthy
  // falsy

  const currentPl = document.getElementById("PlayerID").innerHTML;
	if (event.target.innerHTML == "") {
		event.target.innerHTML = currentPl;
		changePl();
		validation();
	} else {
		alert("This box is occupied, pick another one!");
	}
}

function initHandlers () {
  document.getElementById("box1").addEventListener("click", handleClick)
  document.getElementById("box2").addEventListener("click", handleClick)
  document.getElementById("box3").addEventListener("click", handleClick)
  document.getElementById("box4").addEventListener("click", handleClick)
  document.getElementById("box5").addEventListener("click", handleClick)
  document.getElementById("box6").addEventListener("click", handleClick)
  document.getElementById("box7").addEventListener("click", handleClick)
  document.getElementById("box8").addEventListener("click", handleClick)
  document.getElementById("box9").addEventListener("click", handleClick)
}

function validation () {
	const board = {
		box1: document.getElementById("box1").innerHTML,
		box2: document.getElementById("box2").innerHTML,
		box3: document.getElementById("box3").innerHTML,
		box4: document.getElementById("box4").innerHTML,
		box5: document.getElementById("box5").innerHTML,
		box6: document.getElementById("box6").innerHTML,
		box7: document.getElementById("box7").innerHTML,
		box8: document.getElementById("box8").innerHTML,
		box9: document.getElementById("box9").innerHTML,
	}

	if (board.box1 !== "") {
		//Line 1
		if (board.box1 == board.box2 && board.box2 == board.box3) {
		  alert(`Victory for player ${board.box1}`)
			stopGame = true;
			return;
		} 
		//Column 1
		if (board.box1 == board.box4 && board.box4 == board.box7) {
			alert(`Victory for player ${board.box1}`)
			stopGame = true;
			return;	
		}
		//Diagonal 1-9
		if (board.box1 == board.box5 && board.box5 == board.box9) {
			alert(`Victory for player ${board.box1}`)
			stopGame = true;
			return;
		}
	}
	if (board.box4 !== "") {
		//Line 2
		if (board.box4 == board.box5 && board.box5 == board.box6) {
			alert(`Victory for player ${board.box4}`)
			stopGame = true;
			return;
		}
	}
	if (board.box7 !== "") {
		//Line 3
		if (board.box7 == board.box8 && board.box8 == board.box9) {
			alert(`Victory for player ${board.box7}`)
			stopGame = true;
			return;
		}
		//Diagonal 7-3 3-7
		if (board.box7 == board.box5 && board.box5 == board.box3) {
			alert(`Victory for player ${board.box7}`)
			stopGame = true;
			return;
		}
	}
  if (board.box2 !== "") {
		//Column 2
		if (board.box2 == board.box5 && board.box5 == board.box8) {
			alert(`Victory for player ${board.box2}`)
			stopGame = true;
			return;
		}
	}
  if (board.box3 !== "") {
		//Column 3
		if (board.box3 == board.box6 && board.box6 == board.box9) {
			alert(`Victory for player ${board.box3}`)
			stopGame = true;
			return;
		}
	}
}

function reset () {
	location.reload();
}

initHandlers();