function ex1() {
  const char = document.getElementById("ex1_value_char").value;
  /**
   Exercicio 1
   */
  char.toUpperCase()
  const UpperCase = char.toUpperCase();
  let Result = "O caracter é minúsculo"
  if (char === UpperCase) {
    Result = "O caracter é Maiúsculo"
  }
  document.getElementById("ex1_result").innerHTML = Result;
}

function ex2() {
  const x = parseInt(document.getElementById("ex2_value_x").value, 10);
  const y = parseInt(document.getElementById("ex2_value_y").value, 10);

  /**
   Exercicio 2
   */
  let result
  if (x > 0 && y > 0) {
    result = "Q1"
  } else if (x < 0 && y > 0) {
    result = "Q2"
  } else if (x < 0 && y < 0) {
    result = "Q3"
  } else if (x > 0 && y < 0) {
    result = "Q4"
  } else if (x == 0 && y == 0) {
    result = "Origin"
  } else if (x == 0) {
    result = "X Axis"
  } else if (y == 0) {
    result = "Y Axis"
  }
  document.getElementById("ex2_result").innerHTML = result;
}

function ex3() {
  const hour = parseInt(document.getElementById("ex3_value_hour").value, 10);
  /**
   Exercicio 3
   */
  let result = "Hora Inválida"
  if (hour > 0 && hour < 12) {
    result = hour + "AM"
  } else if (hour > 12 && hour < 23) {
    result = (hour - 12) + "PM"
  } else if (hour == 12) {
    result = "12 PM"
  } else if (hour == 24) {
    result = "12 AM"
  }
  document.getElementById("ex3_result").innerHTML = result;
}

function ex4() {
  const type = parseInt(document.getElementById("ex4_value_type").value, 10);
  const years = parseInt(document.getElementById("ex4_value_years").value, 10);

  /**
   Exercicio 4
   */

  let result = "Combinação Inválida"

  if (type == 1 && years < 5) {
    result = "€45"
  } else if (type == 1 && years >= 5) {
    result = "€25"
  } else if (type == 2 && years < 5) {
    result = "€125"
  } else if (type == 2 && years >= 5) {
    result = "€50"
  } else if (type == 3 && years < 5) {
    result = "€150"
  } else if (type == 3 && years >= 5) {
    result = "€75"
  } else if (type == 4 && years < 5) {
    result = "€250"
  } else if (type == 4 && years >= 5) {
    result = "€150"
  };
  document.getElementById("ex4_result").innerHTML = `O Imposto a pagar = ${result}`;
}

function ex5() {
  const lado1 = parseInt(document.getElementById("ex5_value_lado1").value, 10);
  const lado2 = parseInt(document.getElementById("ex5_value_lado2").value, 10);
  const lado3 = parseInt(document.getElementById("ex5_value_lado3").value, 10);
  /**
   Exercicio 5
   */
  let result = "Combinação Inválida"

  if (lado1 === lado2 && lado2 === lado3 && lado1 === lado3) {
    result = "Triângulo Equilátero"
  } else if (lado1 === lado2 || lado1 === lado3 || lado2 === lado3) {
    result = "Triângulo Isóscelos"
  } else if (lado1 != lado2 && lado1 != lado3 && lado2 != lado3) {
    result = "Triângulo Escaleno"
  }
  document.getElementById("ex5_result").innerHTML = result;
}

function ex6() {
  const value1 = parseInt(
    document.getElementById("ex6_value_valor1").value,
    10
  );
  const value2 = parseInt(
    document.getElementById("ex6_value_valor2").value,
    10
  );
  const value3 = parseInt(
    document.getElementById("ex6_value_valor3").value,
    10
  );
  /**
   Exercicio 6
   */
  let max = value3;

  if (value1 > value2 && value1 > value3) {
    max = value1;
  } else if (value2 > value1 && value2 > value3) {
    max = value2;
  };

  document.getElementById("ex6_result").innerHTML = `O Valor maior é ${max}`;
}

function ex7() {
  const value1 = parseInt(
    document.getElementById("ex7_value_valor1").value,
    10
  );
  const value2 = parseInt(
    document.getElementById("ex7_value_valor2").value,
    10
  );
  const value3 = parseInt(
    document.getElementById("ex7_value_valor3").value,
    10
  );
  /**
   Exercicio 7
   */
  let result = `${value1}, ${value2}, ${value3}`

  if (value2 > value1 && value2 > value3 && value1 > value3) {
    result = `${value2}, ${value1}, ${value3}`
  } else if (value2 > value1 && value2 > value3 && value1 < value3) {
    result = `${value2}, ${value3}, ${value1}`
  } else if (value3 > value1 && value3 > value2 && value1 > value2) {
    result = `${value3}, ${value1}, ${value2}`
  } else if (value3 > value1 && value3 > value2 && value1 < value2) {
    result = `${value3}, ${value2}, ${value1}`
  } else if (value1 > value2 && value1 > value3 && value2 < value3) {
    result = `${value1}, ${value3}, ${value2}`
  }
  document.getElementById("ex7_result").innerHTML = result;
}

function ex8() {
  const date = document.getElementById("ex8_value").value;
  /**
   Exercicio 8
    "24/10/20".split("/");
    ["24", "10", "20"].join("-");
   */
  let dateSplitted = date.split("/").map((value) => {return parseInt(value, 10)})
  let day = dateSplitted[0];
  let month = dateSplitted[1];
  let year = dateSplitted[2];

  switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:

      if (day == 31) {
        day = 1
        month += 1
      } else {
        day += 1;
      }
      break;
    case 12:

      if (day >= 31) {
        day = 1
        month = 1
        year += 1
      } else {
        day = day + 1;
      }
      break;
    case 2:

      if (day >= 28) {
        day = 1
        month += 1
      } else {
        day = day + 1;
      }
      break;

    case 4:
    case 6:
    case 9:
    case 11:

      if (day >= 30) {
        day = 1
        month += 1
      } else {
        day = day + 1;
      }
      break;
    default:
      break;
    }
    result = [day, month, year].join("/");
  document.getElementById("ex8_result").innerHTML = result;
}

function ex9() {
  /**
   Exercicio 9
   
  example:
   const value = prompt("Como te chamas?")
   alert(`olá ${value}`)*/

}
